package com.gekox.gekosLasers.world;

import java.util.Random;

import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.reference.ConfigSettings;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.common.IWorldGenerator;

/**
 * Handles all world generation used in the mod
 * @author Xavier Hunt
 *
 */

public class ModWorldGeneration implements IWorldGenerator {

	private WorldGenerator laserOreGenSingle = new WorldGenSingleMinable(ModBlocks.laserOreBlock);
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
			IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		
		// Check dimension
		switch(world.provider.dimensionId) {
		
		// Overworld
		case(0):
			this.runGenerator(laserOreGenSingle, world, random, chunkX, chunkZ, ConfigSettings.LASERITE_OREGEN_CHANCE, 0, 16);
			break;
		
		// Nether
		case(-1):
			
			break;
		
		// End
		case(1):
			
			break;
		}	
	}
	
	private void runGenerator(WorldGenerator generator, World world, Random rand, int chunk_X, int chunk_Z, int chancesToSpawn, int minHeight, int maxHeight) {
		if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
			throw new IllegalArgumentException("Illegal Height Arguments for WorldGenerator");

		int heightDiff = maxHeight - minHeight + 1;
		for (int i = 0; i < chancesToSpawn; i ++) {
			int x = chunk_X * 16 + rand.nextInt(16);
			int y = minHeight + rand.nextInt(heightDiff);
			int z = chunk_Z * 16 + rand.nextInt(16);
			generator.generate(world, rand, x, y, z);
		}
	}
}
