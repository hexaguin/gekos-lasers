package com.gekox.gekosLasers.blocks;

import java.util.Random;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

// Laserite Ore

public class BlockLaseriteOre extends BlockGL {
	
	private float hardness = 2.0f;
	private float resistance = 10.0f;
	private float lightLevel = 0.5f;
	
	private float leastExpGained = 1.0f;
	private float mostExpGained = 2.0f;
	
	private int leastDropped = 2;
	private int mostDropped = 6;
	
	public BlockLaseriteOre() {
		super(Material.rock);
		
		this.setBlockName(Names.Blocks.LASERITE_ORE);
		
		this.setHardness(hardness);
		this.setResistance(resistance);
		this.setLightLevel(lightLevel);
		this.setHarvestLevel("pickaxe", 2);		// Iron pick
		
	}
	
	@Override
	public Item getItemDropped(int meta, Random rand, int fortune) {
		return ModItems.itemLaserite;
	}
	
	@Override
	public int quantityDropped(int meta, int fortune, Random rand) {
		return 1;
	}

}
