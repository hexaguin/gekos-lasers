package com.gekox.gekosLasers.blocks.tileEntity;

import java.util.ArrayList;
import java.util.List;

import com.gekox.gekosLasers.entity.projectile.laser.*;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.LogHelper;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyHandler;
import cofh.lib.util.position.IRotateableTile;

public class TileEntityRelay extends TileEntityGL implements IEnergyHandler, IInventory {
	
private ItemLaserGun laserThing;
	
	private int storageAmount = ConfigSettings.RELAY_RF_STORAGE;
	public EnergyStorage storage = new EnergyStorage(storageAmount);
	private int energySentPerShot = storageAmount / 10;
	
	protected ItemStack[] inventory;
	
	private int updateFrequency = ConfigSettings.RELAY_UPDATE_FREQUENCY;
	private int currentUpdate = 0;
	
	// NBT names
	private final String TAG_MAX_ENERGY = "maxEnergy";
	private final String TAG_STORED_ENERGY = "storedEnergy";
	private final String TAG_MAX_IO = "maxIO";
	private final String TAG_INVENTORY = "inventory";
	private final String TAG_COOLDOWN = "cooldown";
	
	
	public TileEntityRelay() {
		super();
		this.inventory = new ItemStack[1];
		storage.setMaxTransfer(storageAmount);
		storage.setMaxExtract(energySentPerShot);
	}
	
	public boolean isGettingRedstoneSignal() {
		return worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
	}
	
	/* Update Entity */
	@Override
	public void updateEntity() {
		
		if(this.worldObj.isRemote) {
			return;
		}
		
		this.currentUpdate++;
		
		if(this.currentUpdate == this.updateFrequency) {
			this.currentUpdate = 0;
			
			if(worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
				return;
			
			if(this.inventory[0] != null) {
				
				shoot(createNewLaser());
			}
			
			if(this.getEnergyStored() > energySentPerShot) {
				shoot(createNewLaser());
			}
		}
	}
	
	private ProjectileLaser createNewLaser() {
		ProjectileLaser laser = new ProjectileLaserRed(worldObj);
		laser.numOfThingsBroken = 4;
		laser.blockBreakRadius = 0;
		
		// Attach item
		if(this.inventory[0] != null && laser.getAttachedItemStack() == null) {
			laser.attachItemStack(inventory[0]);
			this.inventory[0] = null;
			//LogHelper.info("Attaching item to laser: " + laser.getAttachedItemStack());
		}
		
		// Attach power
		if(this.getEnergyStored() > 0 && !(laser.getAttachedPower() > energySentPerShot)) {
			laser.attachPower(this.energySentPerShot);
			this.extractEnergy(orientation.getOpposite(), this.energySentPerShot, false);
			//LogHelper.info(String.format("Attached %d power to laser. I have %d left!", laser.getAttachedPower(), this.getEnergyStored(ForgeDirection.DOWN)));
		}
		
		return laser;
	}
	
	public void shoot(ProjectileLaser laser) {
		
		laser.setPosition(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5);
		
		switch(this.orientation) {
		
		case DOWN:
			laser.setThrowableHeading(0, yCoord - 1, 0, -1, 0);
			laser.setPosition(xCoord + 0.5, yCoord - 0.5, zCoord + 0.5);
			break;
			
		case UP:
			laser.setThrowableHeading(0, yCoord + 1, 0, 1, 0);
			break;
			
		case NORTH:
			laser.setThrowableHeading(0, 0, zCoord - 1, -1, 0);
			break;
			
		case SOUTH:
			laser.setThrowableHeading(0, 0, zCoord + 1, 1, 0);
			break;
			
		case WEST:
			laser.setThrowableHeading(xCoord - 1, 0, 0, -1, 0);
			break;
			
		case EAST:
			laser.setThrowableHeading(xCoord + 1, 0, 0, 1, 0);
			break;
			
		default:
			break;
		
		}
		
		worldObj.spawnEntityInWorld(laser);
		//LogHelper.info(laser);
		//worldObj.playSoundEffect(xCoord, yCoord, zCoord, Reference.RESOURCE_PREFIX + "laser.drone", 0.1f, 1);
		
	}

	/* IEnergyConnection */
	@Override
	public boolean canConnectEnergy(ForgeDirection from) {
		return true;
	}

	/* IEnergyReceiver */
	@Override
	public int receiveEnergy(ForgeDirection from, int maxReceive, boolean simulate) {
		return storage.receiveEnergy(maxReceive, simulate);
	}

	/* IEnergyProvider */
	@Override
	public int extractEnergy(ForgeDirection from, int maxExtract, boolean simulate) {
		return storage.extractEnergy(maxExtract, simulate);
	}
	
	public void setEnergyStored(int energy) {
		storage.setEnergyStored(energy);
	}

	/* IEnergyReceiver and IEnergyProvider */
	@Override
	public int getEnergyStored(ForgeDirection from) {
		return storage.getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored(ForgeDirection from) {
		return storage.getMaxEnergyStored();
	}
	
	public int getEnergyStored() {
		return this.getEnergyStored(ForgeDirection.DOWN);
	}

	public int getMaxEnergyStored() {
		return this.getMaxEnergyStored(ForgeDirection.DOWN);
	}

	/* IInventory */
	
	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return this.inventory[slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount) {

		ItemStack stack = this.getStackInSlot(slot);
		
		if(stack != null) {
			if(stack.stackSize <= amount) {
				setInventorySlotContents(slot, null);
			}
			
			else {
				stack = stack.splitStack(amount);
				if(stack.stackSize == 0) {
					setInventorySlotContents(slot, null);
				}
			}
		}
		
		return stack;
		
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		
		return null;
        
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack) {
		
		this.inventory[slot] = stack;
		
        if (stack != null && stack.stackSize > getInventoryStackLimit()) {
            stack.stackSize = getInventoryStackLimit();
        }
		
	}

	@Override
	public String getInventoryName() {
		return null;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return (this.worldObj.getTileEntity(xCoord, yCoord, zCoord) == this) && (player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64);
	}

	@Override
	public void openInventory() {
		// No-op
		
	}

	@Override
	public void closeInventory() {
		// No-op
		
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack stack) {
		return true;
		
	}

}
