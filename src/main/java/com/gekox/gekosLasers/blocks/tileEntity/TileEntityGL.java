package com.gekox.gekosLasers.blocks.tileEntity;

import java.util.UUID;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.network.MessageTileEntityGL;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.common.UsernameCache;

public class TileEntityGL extends TileEntity {

	protected ForgeDirection orientation;
	protected UUID ownerUUID;
	
	private String TAG_DIRECTION = "direction";
	private String TAG_OWNER_UUID_MOST_SIG = "ownerUUIDMostSig";
	private String TAG_OWNER_UUID_LEAST_SIG = "ownerUUIDLeastSig";
	
	public TileEntityGL() {
		this.orientation = ForgeDirection.NORTH;
		this.ownerUUID = null;
	}

	public ForgeDirection getOrientation() {
		return orientation;
	}

	public void setOrientation(ForgeDirection orientation) {
		this.orientation = orientation;
	}
	
	public void setOrientation(int orientation) {
		this.orientation = ForgeDirection.getOrientation(orientation);
	}

	public String getOwner() {
		if (ownerUUID != null) {
			return UsernameCache.getLastKnownUsername(ownerUUID);
		}

		return "Unknown";
	}
	
	public UUID getOwnerUUID() {
		return ownerUUID;
	}
	
	public void setOwnerUUID(EntityPlayer player) {
		this.ownerUUID = player.getPersistentID();
	}
	
	public void setOwnerUUID(UUID ownerUUID) {
		this.ownerUUID = ownerUUID;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound) {
		super.readFromNBT(nbtTagCompound);

		if (nbtTagCompound.hasKey(TAG_DIRECTION)) {
			this.setOrientation(ForgeDirection.getOrientation(nbtTagCompound.getByte(TAG_DIRECTION)));
		}

		if (nbtTagCompound.hasKey(TAG_OWNER_UUID_MOST_SIG) && nbtTagCompound.hasKey(TAG_OWNER_UUID_LEAST_SIG)) {
			this.ownerUUID = new UUID(nbtTagCompound.getLong(TAG_OWNER_UUID_MOST_SIG), nbtTagCompound.getLong(TAG_OWNER_UUID_LEAST_SIG));
		}
	}
	
	public boolean hasOwner() {
		return ownerUUID != null;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound) 
	{
		super.writeToNBT(nbtTagCompound);

		nbtTagCompound.setByte(TAG_DIRECTION, (byte)orientation.ordinal());

		if (this.hasOwner()) {
			nbtTagCompound.setLong(TAG_OWNER_UUID_MOST_SIG, ownerUUID.getMostSignificantBits());
			nbtTagCompound.setLong(TAG_OWNER_UUID_LEAST_SIG, ownerUUID.getLeastSignificantBits());
		}
	}
	
	@Override
	public Packet getDescriptionPacket() {
		return GekosLasers.network.getPacketFrom(new MessageTileEntityGL(this));
	}
	
}
