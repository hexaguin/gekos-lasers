package com.gekox.gekosLasers.blocks.tileEntity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyHandler;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.network.UpdateEnergyMessage;
import com.gekox.gekosLasers.network.UpdateTurretMessage;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LaserUtils;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.TurretUtil;

/*

A lot of this code has come from the OpenModularTurrets mod

*/
public class TileEntityTurret extends TileEntityGL implements IEnergyHandler, IInventory {

	private ItemLaserGun laserThing;
	
	private int storageAmount = 200000;
	public EnergyStorage storage = new EnergyStorage(storageAmount);
	
	protected ItemStack[] inventory;
	protected List<String> trustedPlayers = new ArrayList<String>();;
	
	protected final int coolDown = 20;
	protected int currentCooldown = 0;
	
	protected int energyPerUse = 200;
	protected int energyPerTick = 5;
	
	private ProjectileLaser laser;
	private Entity target;
	
	public boolean attackMobs;
	public boolean attackPlayers;
	public int range = 14;
	
	// These are 10x the actual amount
	private int accuracyMod = 10;
	private int damageMod = 10;
	private int cooldownMod = 10;
	
	public boolean isActive = false;
	
	// NBT names
	private final String TAG_MAX_ENERGY = "maxEnergy";
	private final String TAG_STORED_ENERGY = "storedEnergy";
	private final String TAG_MAX_IO = "maxIO";
	//private final String TAG_TRUSTED = "trusted";
	private final String TAG_INVENTORY = "inventory";
	private final String TAG_COOLDOWN = "cooldown";
	private final String TAG_ATTACK_MOBS = "attackMobs";
	private final String TAG_ATTACK_PLAYERS = "attackPlayers";
//	private final String TAG_TARGET_ID = "target";
	private final String TAG_ACCURACY_MOD = "accuracyMod";
	private final String TAG_DAMAGE_MOD = "damageMod";
	private final String TAG_COOLDOWN_MOD = "cooldownMod";
	private final String TAG_RANGE = "range";
	
	public TileEntityTurret() {
		super();
		this.inventory = new ItemStack[1];
	}
	
	public boolean isGettingRedstoneSignal() {
		return worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
	}
	
	/* NBT and Packet stuff */
	
	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound tag = new NBTTagCompound();
		this.writeToNBT(tag);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 2, tag);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
		this.readFromNBT(packet.func_148857_g());
		this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		this.markDirty();
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {

		super.readFromNBT(nbt);
		
		this.storage.setCapacity(nbt.getInteger(this.TAG_MAX_ENERGY));
		this.storage.setEnergyStored(nbt.getInteger(this.TAG_STORED_ENERGY));
		this.storage.setMaxReceive(nbt.getInteger(this.TAG_MAX_IO));
		
//		this.owner = nbt.getString(this.TAG_OWNER);
//		this.trustedPlayers = buildTrustedPlayersFromNBT(nbt.getTagList(TAG_TRUSTED, 8));
		
		this.currentCooldown = nbt.getInteger(this.TAG_COOLDOWN);
		
//		this.changeTarget(nbt.getInteger(TAG_TARGET_ID));
		this.attackMobs = nbt.getBoolean(TAG_ATTACK_MOBS);
		this.attackPlayers = nbt.getBoolean(TAG_ATTACK_PLAYERS);
		
		this.damageMod = nbt.getInteger(TAG_DAMAGE_MOD);
		this.accuracyMod = nbt.getInteger(TAG_ACCURACY_MOD);
		this.cooldownMod = nbt.getInteger(TAG_COOLDOWN_MOD);
		
		this.range = nbt.getInteger(TAG_RANGE);
		
		NBTTagList tagList = nbt.getTagList(this.TAG_INVENTORY, 10);
		
		for(int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound tag = tagList.getCompoundTagAt(i);
			byte slot = tag.getByte("slot");
			
			if(slot >= 0 && slot < this.inventory.length) {
				this.inventory[slot] = ItemStack.loadItemStackFromNBT(tag);
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {

		super.writeToNBT(nbt);
		
		nbt.setInteger(this.TAG_MAX_ENERGY, this.storage.getMaxEnergyStored());
		nbt.setInteger(this.TAG_STORED_ENERGY, this.getEnergyStored());
		nbt.setInteger(this.TAG_MAX_IO, this.storage.getMaxReceive());
//		nbt.setString(this.TAG_OWNER, this.owner);
//		nbt.setTag(this.TAG_TRUSTED, getTrustedPlayersAsNBT());
		
		nbt.setInteger(TAG_COOLDOWN, this.currentCooldown);
		
//		nbt.setInteger(TAG_TARGET_ID, this.getTargetValue());
		nbt.setBoolean(TAG_ATTACK_MOBS, this.attackMobs);
		nbt.setBoolean(TAG_ATTACK_PLAYERS, this.attackPlayers);
		
		nbt.setInteger(TAG_DAMAGE_MOD, this.damageMod);
		nbt.setInteger(TAG_ACCURACY_MOD, this.accuracyMod);
		nbt.setInteger(TAG_COOLDOWN_MOD, this.cooldownMod);
		
		nbt.setInteger(TAG_RANGE, this.range);
		
		NBTTagList itemList = new NBTTagList();
		
		for(int i = 0; i < this.inventory.length; i++) {
			ItemStack stack = this.getStackInSlot(i);
			
			if(stack != null) {
				NBTTagCompound tag = new NBTTagCompound();
				tag.setByte("slot", (byte)i);
				stack.writeToNBT(tag);
				itemList.appendTag(tag);
			}
		}
		
		nbt.setTag(this.TAG_INVENTORY, itemList);
	}
	
	public void addTrustedPlayer(String name) {
        trustedPlayers.add(name);
    }

    public void removeTrustedPlayer(String name) {
        trustedPlayers.remove(name);
    }
	
    public List<String> getTrustedPlayers() {
        return trustedPlayers;
    }

    private NBTTagList getTrustedPlayersAsNBT() {
        NBTTagList nbt = new NBTTagList();

        for (String trustedPlayer : trustedPlayers) {
            nbt.appendTag(new NBTTagString(trustedPlayer));
        }

        return nbt;
    }
    
    public int getBaseEnergyPerTick() {
    	return this.energyPerTick;
    }
    
    public int getEnergyPerTick() {
    	return TurretUtil.getRFUsePerTick(this.energyPerTick, this.range);
    }
    
    public int getBaseEnergyPerShot() {
    	return this.energyPerUse;
    }
    
    public int getEnergyPerShot() {
    	return TurretUtil.getRFUsePerShot(this.energyPerUse, this.damageMod, this.accuracyMod, this.cooldownMod);
    }

    private List<String> buildTrustedPlayersFromNBT(NBTTagList nbt) {
        List<String> trusted_players = new ArrayList<String>();

        for (int i = 0; i < nbt.tagCount(); i++) {
            trusted_players.add(nbt.getStringTagAt(i));
        }

        return trusted_players;
    }
    
	public EnergyStorage getStorage() {
		return this.storage;
	}
	
	/* Update Entity */
	@Override
	public void updateEntity() {
		super.updateEntity();
		
		if(this.worldObj.isRemote) {
			return;
		}
		
		// Use some energy
		if(!this.isGettingRedstoneSignal()) {
			this.extractEnergy(ForgeDirection.DOWN, this.getEnergyPerTick(), false);
		}
		
		// See if there's power in the turret, and if so, set it to be active
		this.isActive = this.getEnergyStored() > 0;
		
		if(this.currentCooldown > 0) {
			this.currentCooldown--;
		}
		
		//System.out.println("Cooldown:" + currentCooldown);
				
		if(this.inventory[0] != null && this.inventory[0].getItem() instanceof ItemLaserGun) {
			
			ItemStack stack = this.inventory[0];
			ItemLaserGun gun = (ItemLaserGun)stack.getItem();
			
			if(gun.getCurrentCooldown(stack) > 0)
				gun.setCooldown(stack, gun.getCurrentCooldown(stack) - 1);
			
			if((this.getEnergyStored() > 0) && (gun.getEnergyStored(stack) < gun.getMaxEnergyStored(stack))) {
				
				gun.receiveEnergy(stack, gun.maxTransfer, false);
				this.extractEnergy(ForgeDirection.DOWN, gun.maxTransfer, false);
			}
			
			// Fire if there is no redstone signal
			if(!this.isGettingRedstoneSignal() && this.getEnergyStored() > this.getEnergyPerShot())
				fire(gun, stack);
			
		}
		
		// Send a message about the energy in this turret
		UpdateEnergyMessage energyMessage = new UpdateEnergyMessage(this.xCoord, this.yCoord, this.zCoord, this.getEnergyStored());
		GekosLasers.network.sendToAll(energyMessage);
		
	}

	private void fire(ItemLaserGun gun, ItemStack stack) {
		
		if(this.currentCooldown > 0) {
			return;
		}
		
		// Always reset the target before firing
		target = getTarget();
		
		if(target == null) {
			return;
		}
		
		LogHelper.debug("Firing turret: " + this.toString());
		
		double d0 = target.posX - this.xCoord - 0.5;
		double d1 = target.posY + target.getEyeHeight() - (this.yCoord + 1);
		double d2 = target.posZ - this.zCoord - 0.5;
		
		laser = LaserUtils.determineLaser(getWorldObj(), Color.color[gun.getColorID(stack)]);
		laser.setupLaser((int)Math.max(gun.getLaserDamage() * (this.getDamageMod() / 10.0f), 1),
				gun.getLaserStrength(), 
				gun.getLaserBlockBreakRadius(), 
				gun.isLaserDoExplosionDamage(), 
				gun.getLaserExplosionSize(), 
				gun.isLaserDoFireDamage(),
				gun.getLaserBurnTime(),
				gun.isLaserDoTeleport());
		
		laser.numOfThingsBroken = gun.getNumOfThingsBroken();
		
		laser.setThrowableHeading(d0 + target.motionX, d1 + target.motionY, d2 + target.motionZ, 1, 2 - (this.getAccuracyMod() / 10.0f));
		laser.setPosition(xCoord + 0.5, yCoord + 1, zCoord + 0.5);
		
		this.worldObj.spawnEntityInWorld(laser);
		this.worldObj.playSoundEffect(xCoord, yCoord, zCoord, Reference.RESOURCE_PREFIX + "laser.pew", 0.5f, 1);
		
		this.currentCooldown = (int) (gun.getCoolDown() * 1.5f / (this.getCooldownMod() / 10.0f));
		gun.extractEnergy(stack, gun.energyPerUse, false);
		this.extractEnergy(ForgeDirection.DOWN, this.getEnergyPerShot(), false);
		
		
	}
	
	// Change the target
	public void changeTarget(int target) {
		
		switch(target) {
		
		// None
		case(0):
			this.attackMobs = false;
			this.attackPlayers = false;
			break;
			
		// Mobs
		case(1):
			this.attackMobs = true;
			this.attackPlayers = false;
			break;
			
		// Players
		case(2):
			this.attackMobs = false;
			this.attackPlayers = true;
			break;
		
		// Mobs and Players
		case(3):
			this.attackMobs = true;
			this.attackPlayers = true;
			break;
		}
		
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		markDirty();
		this.target = this.getTarget();
		
		LogHelper.info("ChangingTarget - Got a value of " + target);
		LogHelper.info("Setting Mobs to " + this.attackMobs + " and Players to " + this.attackPlayers);
	}
	
	// Gets a value based on the target
	public int getTargetValue() {
		
		int val = 0;
		
		if(!this.attackMobs && !this.attackPlayers)
			val = 0;
		
		if(this.attackMobs && !this.attackPlayers)
			val = 1;
		
		if(!this.attackMobs && this.attackPlayers)
			val = 2;
		
		if(this.attackMobs && this.attackPlayers)
			val = 3;
		
		LogHelper.info("GetTargetValue - M: " + this.attackMobs + " P: " + this.attackPlayers);
		LogHelper.info("ID generated: " + val);
		
		return val;
		
	}
	
	private Entity getTarget() {
		return TurretUtil.getTarget(this, worldObj, xCoord, yCoord, zCoord, range);
	}

	public int getAccuracyMod() {
		return accuracyMod;
	}

	public void setAccuracyMod(int accuracyMod) {
		this.accuracyMod = accuracyMod;
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		markDirty();
	}

	public int getDamageMod() {
		return damageMod;
	}

	public void setDamageMod(int damageMod) {
		this.damageMod = damageMod;
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		markDirty();
	}

	public int getCooldownMod() {
		return cooldownMod;
	}

	public void setCooldownMod(int cooldownMod) {
		this.cooldownMod = cooldownMod;
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		markDirty();
	}

	/* IEnergyConnection */
	@Override
	public boolean canConnectEnergy(ForgeDirection from) {
		return true;
	}

	/* IEnergyReceiver */
	@Override
	public int receiveEnergy(ForgeDirection from, int maxReceive, boolean simulate) {
		return storage.receiveEnergy(maxReceive, simulate);
	}

	/* IEnergyProvider */
	@Override
	public int extractEnergy(ForgeDirection from, int maxExtract, boolean simulate) {
		return storage.extractEnergy(maxExtract, simulate);
	}
	
	public void setEnergyStored(int energy) {
		storage.setEnergyStored(energy);
	}

	/* IEnergyReceiver and IEnergyProvider */
	@Override
	public int getEnergyStored(ForgeDirection from) {
		return storage.getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored(ForgeDirection from) {
		return storage.getMaxEnergyStored();
	}
	
	public int getEnergyStored() {
		return this.getEnergyStored(ForgeDirection.DOWN);
	}

	public int getMaxEnergyStored() {
		return this.getMaxEnergyStored(ForgeDirection.DOWN);
	}

	/* IInventory */
	
	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return this.inventory[slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount) {

		ItemStack stack = this.getStackInSlot(slot);
		
		if(stack != null) {
			if(stack.stackSize <= amount) {
				setInventorySlotContents(slot, null);
			}
			
			else {
				stack = stack.splitStack(amount);
				if(stack.stackSize == 0) {
					setInventorySlotContents(slot, null);
				}
			}
		}
		
		return stack;
		
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		
		ItemStack stack = getStackInSlot(slot);
		
        if (stack != null) {
            setInventorySlotContents(slot, null);
        }
        
        return stack;
        
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack) {
		
		this.inventory[slot] = stack;
		
        if (stack != null && stack.stackSize > getInventoryStackLimit()) {
            stack.stackSize = getInventoryStackLimit();
        }
		
	}

	@Override
	public String getInventoryName() {
		return this.hasOwner() ? this.getOwner() + "'s Laserite Turret" : "Unowned Laserite Turret";
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return (this.worldObj.getTileEntity(xCoord, yCoord, zCoord) == this) && (player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64);
	}

	@Override
	public void openInventory() {
		// No-op
		
	}

	@Override
	public void closeInventory() {
		// No-op
		
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack stack) {
		if(stack.getItem() instanceof ItemLaserGun) {
			return true;
		}
		
		return false;
		
	}
	
}
