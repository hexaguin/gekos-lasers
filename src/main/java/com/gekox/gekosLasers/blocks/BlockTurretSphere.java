package com.gekox.gekosLasers.blocks;

import java.util.ArrayList;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurretSphere;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.RenderIDs;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockTurretSphere extends BlockTileEntityGL {

	public BlockTurretSphere() {
		super();	
		this.setBlockName(Names.Blocks.TURRET_BLOCK_SPHERE);
		this.setHardness(2);
		this.setResistance(10);
		this.setHarvestLevel("pickaxe", 2);		// Iron pick
		//minX, minY, minZ, maxX, maxY, maxZ
		this.setBlockBounds(0.25f, 0.25f, 0.25f, 0.75f, 0.75f, 0.75f);
		this.setCreativeTab(null);
	}
	
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new TileEntityTurretSphere();
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return RenderIDs.turret_sphere;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
		//super.onBlockPlacedBy(world, x, y, z, entity, stack);
	}

}
