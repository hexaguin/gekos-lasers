package com.gekox.gekosLasers.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import com.gekox.gekosLasers.creativeTab.CreativeTabGL;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Meta item base class
 * @author Xavier Hunt
 *
 */

public class MetaBlockGL extends Block {
	
	private IIcon[] icons;
	private int numOfBlocks;
	/** Names to append to the end of the meta item */
//	public String[] names = {"0",
//							  "1",
//							  "2",
//							  "3",
//							  "4",
//							  "5",
//							  "6",
//							  "7",
//							  "8",
//							  "9",
//							  "10",
//							  "11",
//							  "12",
//							  "13",
//							  "14",
//							  "15"};
	
	/**
	 * Constructor. <br>
	 * Sets a new meta block with a full 16 sub-blocks
	 * 
	 */
	public MetaBlockGL() {
		this(16, Material.rock);
	}
	
	public MetaBlockGL(Material material) {
		this(16, material);
		this.setCreativeTab(CreativeTabGL.GL_TAB);
	}
	
	/**
	 * Constructor
	 * @param items: the number of sub-blocks
	 */
	public MetaBlockGL(int items, Material material) {
		super(material);
		icons = new IIcon[items];
		this.numOfBlocks = icons.length;
		this.setCreativeTab(CreativeTabGL.GL_TAB);
	}
	
	public int getNumberOfItems() {
		return this.numOfBlocks;
	}
	
	public String getUnlocalizedName(int meta) {
		return String.format("tile.%s%s_%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()), meta);
	}
	
	/**
	 * Returns the unlocalized name, in an unwrapped form
	 * @param unlocalizedName
	 * @return
	 */
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
	// Params: side, meta
	@Override
	public IIcon getIcon(int side, int meta) {
		meta = (meta > numOfBlocks - 1) ? numOfBlocks : ((meta < 0) ? 0 : meta);
		return this.icons[meta];
	}
	
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list) {
		for (int i = 0; i < numOfBlocks; i ++) {
	    	list.add(new ItemStack(item, 1, i));
	    }
	}
	
	/**
	 * Registers the item's icon
	 */
	@Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
		for (int i = 0; i < numOfBlocks; i++) {
			this.icons[i] = iconRegister.registerIcon(this.getUnlocalizedName(i).substring(this.getUnlocalizedName(i).indexOf(".") + 1));
		}
	}
}
