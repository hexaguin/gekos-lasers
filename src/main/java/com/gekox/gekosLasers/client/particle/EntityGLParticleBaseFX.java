package com.gekox.gekosLasers.client.particle;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

@SideOnly(Side.CLIENT)
public class EntityGLParticleBaseFX extends EntityFX {

	private double gravity;
	private double friction;
	private Color color = Color.BLACK;
	private static final ResourceLocation resourceLoc = new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, LaserReference.PARTICLE_TEXTURE);
	
	public EntityGLParticleBaseFX(World world, double posX, double posY, double posZ, double motX, double motY, double motZ, float scale, int maxAge, double gravity, double friction, Color color) {
		super(world, posX, posY, posZ, motX, motY, motZ);
		
		this.motionX = motX;
		this.motionY = motY;
		this.motionZ = motZ;
		this.particleMaxAge = maxAge;
		this.particleScale = scale;
		
		this.friction = friction;
		this.gravity = gravity;
		
		this.color = color;
		
	}
	
	public EntityGLParticleBaseFX(World world, double posX, double posY, double posZ, double motX, double motY, double motZ, float scale, int maxAge, Color color) {
		super(world, posX, posY, posZ, motX, motY, motZ);
		
		this.motionX = motX;
		this.motionY = motY;
		this.motionZ = motZ;
		this.particleMaxAge = maxAge;
		this.particleScale = scale;
		
		this.friction = 1;
		this.gravity = 0;
		
		this.color = color;
		
	}
	
	public EntityGLParticleBaseFX(World world, double posX, double posY, double posZ, double motX, double motY, double motZ) {
		super(world, posX, posY, posZ, motX, motY, motZ);
	}
	
	@Override
	public int getFXLayer() {
		return 3;
	}
	
	@Override
	public void onUpdate() {
		
		// Check if we need to kill this particle
		if(this.particleAge > this.particleMaxAge) {
			this.setDead();
		}
		
		// Check client render settings, and disable this particle if the settings say to do so
		if(Minecraft.getMinecraft().gameSettings.particleSetting == 2) {
			this.setDead();
		}
		
		// Apply movement to the particle (ONLY if the world is a client world)
		if(this.worldObj.isRemote) {
			this.doMotion();
		}
		
		// Age the particle
		this.particleAge++;
		
	}
	
	private void doMotion() {
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		
		this.motionX *= this.friction;
		this.motionY *= this.friction;
		this.motionZ *= this.friction;
		
		this.motionY -= this.gravity;
		this.moveEntity(this.motionX, this.motionY, this.motionZ);
	}
	
	// Actually render the particle
	@Override
	public void renderParticle(Tessellator tessellator, float par2, float par3, float par4, float par5, float par6, float par7){
		
		float PScale = 0.1F * this.particleScale;
		
		float x=(float)(this.prevPosX+(this.posX-this.prevPosX)*par2-interpPosX);
		float y=(float)(this.prevPosY+(this.posY-this.prevPosY)*par2-interpPosY);
		float z=(float)(this.prevPosZ+(this.posZ-this.prevPosZ)*par2-interpPosZ);
		
		float alphaPercent = ((float)this.particleMaxAge / (float)this.particleAge - 1) * 0.5f;
//		System.out.print(alphaPercent);
//		System.out.print(" : ");
//		System.out.print((int) (color.a * alphaPercent));
//		System.out.print("\n");
		
		GL11.glPushMatrix();
		
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		
		tessellator.startDrawingQuads();
		
		Minecraft.getMinecraft().renderEngine.bindTexture(resourceLoc);
		
		tessellator.setColorRGBA(color.r, color.g, color.b, (int) (color.a * alphaPercent));
		
		tessellator.setBrightness(240);

		tessellator.addVertexWithUV((double)(x-par3*PScale-par6*PScale), (double)(y-par4*PScale), (double)(z-par5*PScale-par7*PScale), 0, 0);
		tessellator.addVertexWithUV((double)(x-par3*PScale+par6*PScale), (double)(y+par4*PScale), (double)(z-par5*PScale+par7*PScale), 1, 0);
		tessellator.addVertexWithUV((double)(x+par3*PScale+par6*PScale), (double)(y+par4*PScale), (double)(z+par5*PScale+par7*PScale), 1, 1);
		tessellator.addVertexWithUV((double)(x+par3*PScale-par6*PScale), (double)(y-par4*PScale), (double)(z+par5*PScale-par7*PScale), 0, 1);

		tessellator.draw();
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		
		GL11.glPopMatrix();
		
	}
	
}
