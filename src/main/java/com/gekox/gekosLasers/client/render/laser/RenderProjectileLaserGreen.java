package com.gekox.gekosLasers.client.render.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.Entity;

public class RenderProjectileLaserGreen extends RenderProjectileLaser {
	
	@Override
	public void doRender(Entity entity, double d0, double d1, double scale, float f0, float f1) {
		this.setColor(Color.GREEN);
		super.doRender(entity, d0, d1, scale, f0, f1);
		
	}

}
