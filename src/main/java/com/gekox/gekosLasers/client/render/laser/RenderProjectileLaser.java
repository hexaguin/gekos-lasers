package com.gekox.gekosLasers.client.render.laser;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaserBlue;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.client.renderer.entity.RenderLightningBolt;

/**
 * Abstract base class for rendering a laser
 * 
 * @author Xavier Hunt
 *
 */

public class RenderProjectileLaser extends Render {

//	private Color color = Color.WHITE;

//	@Override
//	public void doRender(Entity entity, double d0, double d1, double scale, float f0, float f1) {
//		this.render(entity, d0, d1, scale, f0, f1, Color.WHITE);
//	}
	
	public void setColor(Color color) {
		
	}
	
	@Override
	public void doRender(Entity entity, double d0, double d1, double scale, float f0, float f1) {
		
		if(!(entity instanceof ProjectileLaser))
			return;
		
		ProjectileLaser laser = (ProjectileLaser)entity;
	//	System.out.println(laser);
	//	this.color = laser.getColor();
		
		Color c = laser.getColor();
		
		//System.out.println(laser.getColor());
		
		ResourceLocation resourceLoc = new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, LaserReference.LASER_TEXTURE);
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)d0, (float)d1, (float)scale);
		GL11.glRotatef(entity.prevRotationYaw + (entity.rotationYaw - entity.prevRotationYaw) * f1 - 90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * f1, 0.0F, 0.0F, 1.0F);
		
		Tessellator tessellator = Tessellator.instance;
		
		byte b0 = 0;
		float f2 = 0.0F;
		float f3 = 0.5F;
		float f4 = (float)(0 + b0 * 10) / 32.0F;
		float f5 = (float)(5 + b0 * 10) / 32.0F;
		float f6 = 0.0F;
		float f7 = 0.15625F;
		float f8 = (float)(5 + b0 * 10) / 32.0F;
		float f9 = (float)(10 + b0 * 10) / 32.0F;
		float f10 = 0.05625F;
		float f11 = 0;
		
		//GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		
		tessellator.setBrightness(240);

		if (f11 > 0.0F)
		{
			float f12 = -MathHelper.sin(f11 * 3.0F) * f11;
			GL11.glRotatef(f12, 0.0F, 0.0F, 1.0F);
		}

		GL11.glRotatef(45.0F, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(f10, f10, f10);
		GL11.glTranslatef(-4.0F, 0.0F, 0.0F);
		GL11.glNormal3f(f10, 0.0F, 0.0F);

		for (int i = 0; i < 4; i++) {
			
			GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
			GL11.glNormal3f(0.0F, 0.0F, f10);
			
			tessellator.startDrawingQuads();
			
			Minecraft.getMinecraft().renderEngine.bindTexture(resourceLoc);
			
			tessellator.setColorRGBA(laser.getColor().r, laser.getColor().g, laser.getColor().b, laser.getColor().a);
			
			tessellator.addVertexWithUV(-8, -1, 0, 0, 0);
			tessellator.addVertexWithUV(-8, 1, 0, 0, 1);
			tessellator.addVertexWithUV(8, 1, 0, 1, 1);
			tessellator.addVertexWithUV(8, -1, 0, 1, 0);
			
			tessellator.draw();
		}
		
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		
		GL11.glPopMatrix();
		
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
		// TODO Auto-generated method stub
		return null;
	}

}
