package com.gekox.gekosLasers.client.render.model;

import com.gekox.gekosLasers.reference.Models;

import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelTurretSphere {

	private IModelCustom modelTurretSphere;
	
	public ModelTurretSphere() {
		modelTurretSphere = AdvancedModelLoader.loadModel(Models.TURRET_SPHERE);
	}
	
	public void render() {
		modelTurretSphere.renderAll();
	}
}
