package com.gekox.gekosLasers.client.gui;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.GuiScreen;

public class GUITest extends GuiScreen {

	private int guiWidth = 100;
	private int guiHeight = 100;
	
	@Override
	public void drawScreen(int x, int y, float ticks) {
		
		int guiX = (this.width - guiWidth) / 2;
		int guiY = (this.height - guiHeight) / 2;
		
		GL11.glColor4f(1, 1, 1, 1);
		this.drawDefaultBackground();
		
		super.drawScreen(x, y, ticks);
	}
	
}
