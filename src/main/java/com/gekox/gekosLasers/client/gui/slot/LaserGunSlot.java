package com.gekox.gekosLasers.client.gui.slot;

import com.gekox.gekosLasers.items.ItemLaserGun;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

/**
 * Slot definition for a LaserGun
 * 
 * @author Xav
 *
 */
public class LaserGunSlot extends Slot {

	public LaserGunSlot(IInventory iInventory, int slotIndex, int xPos, int yPos) {
		super(iInventory, slotIndex, xPos, yPos);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		if(stack.getItem() instanceof ItemLaserGun) {
			return true;
		}
		
		return false;
	}
	
}
