package com.gekox.gekosLasers.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cofh.lib.gui.*;
import cofh.lib.gui.element.ElementButtonManaged;
import cofh.lib.gui.element.ElementButtonOption;
import cofh.lib.gui.element.ElementEnergyStored;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.network.UpdateEnergyMessage;
import com.gekox.gekosLasers.network.UpdateTurretMessage;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;
import com.gekox.gekosLasers.utility.TurretUtil;
import com.gekox.gekosLasers.utility.gui.GUILabel;
import com.gekox.gekosLasers.utility.gui.ToggleButton;
import com.gekox.gekosLasers.utility.gui.TurretSlider;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.ForgeDirection;

public class TurretGuiBase extends GuiBase {

	// Mouse position
	private int mouseX;
	private int mouseY;
	
	// The turret that this gui refers to
	private TileEntityTurret turret;
	
	private static ResourceLocation texture = (new ResourceLocation(Reference.RESOURCE_PREFIX + "textures/gui/turretGui.png"));
	
	// Elements
	private ToggleButton buttonToggleTarget;
	private TurretSlider sliderRange;
	private TurretSlider sliderDamage;
	private TurretSlider sliderAccuracy;
	private TurretSlider sliderCooldown;
	
	private GUILabel RFTick;
	private GUILabel RFShot;
	
	public TurretGuiBase(InventoryPlayer inventoryPlayer, TileEntityTurret tileEntity) {
		super(new TurretContainerTier1(inventoryPlayer, tileEntity), texture);

		this.turret = tileEntity;
		this.xSize = 225;
		this.ySize = 200;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void initGui() {
		
		super.initGui();
		
		System.out.println("GUI init");
		
		// Center of the screen
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		
		// Construct and add elements
		
		// Energy
		this.elements.add(new ElementEnergyStored(this, 8, 20, this.turret.storage));
		
		// X value for most of the GUI elements
		int xPos = 115;
		
		// Target button
		buttonToggleTarget = new ToggleButton(this, xPos, 20, 100, 20, this.turret);
		
		List<String> toolTip = new ArrayList<String>();
		toolTip.add("Change the target of this turret");
		
		this.buttonToggleTarget.addTooltip(toolTip);
		
		this.buttonToggleTarget.setValue(0, "None");
		this.buttonToggleTarget.setValue(1, "Mobs");
		this.buttonToggleTarget.setValue(2, "Players");
		this.buttonToggleTarget.setValue(3, "Mobs and Players");
		
		this.buttonToggleTarget.setSelectedIndex(this.turret.getTargetValue());
		
		this.elements.add(buttonToggleTarget);
		
		// Sliders
		this.sliderRange = new TurretSlider(this, xPos, 45, 100, 10, 20, 1, this.turret);
		this.sliderRange.setName("range");
		this.sliderRange.setValue(turret.range);
		this.elements.add(sliderRange);
		
		this.sliderDamage = new TurretSlider(this, xPos, 56, 100, 10, 20, 1, this.turret);
		this.sliderDamage.setName("damage");
		this.sliderDamage.setValue(turret.getDamageMod());
		this.elements.add(sliderDamage);
		
		this.sliderAccuracy = new TurretSlider(this, xPos, 67, 100, 10, 20, 1, this.turret);
		this.sliderAccuracy.setName("accuracy");
		this.sliderAccuracy.setValue(turret.getAccuracyMod());
		this.elements.add(sliderAccuracy);
		
		this.sliderCooldown = new TurretSlider(this, xPos, 78, 100, 10, 20, 1, this.turret);
		this.sliderCooldown.setName("cooldown");
		this.sliderCooldown.setValue(turret.getCooldownMod());
		this.elements.add(sliderCooldown);
		
		// Confirm button
		GuiButton confirmButton = new GuiButton(0, x + xPos, y + 93, 100, 20, "Confirm");
		this.buttonList.add(confirmButton);
		if(!Minecraft.getMinecraft().thePlayer.getDisplayName().equals(turret.getOwner())) {
			confirmButton.enabled = false;
		}
		
		// Labels
		GUILabel infoLabel1 = new GUILabel(this, 10, 65, 100, 10, "RF per tick:");
		this.elements.add(infoLabel1);
		
		RFTick = new GUILabel(this, 10, 75, 50, 10, "");
		this.elements.add(RFTick);
		
		GUILabel infoLabel2 = new GUILabel(this, 10, 85, 100, 10, "RF per shot:");
		this.elements.add(infoLabel2);
		
		RFShot = new GUILabel(this, 10, 95, 50, 10, "");
		this.elements.add(RFShot);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		
		// Draw turret title
		String s = this.turret.hasCustomInventoryName() ? this.turret.getInventoryName() : I18n.format(this.turret.getInventoryName(), new Object[0]);
		this.fontRendererObj.drawString(s, this.xSize / 2 - this.fontRendererObj.getStringWidth(s) / 2, 6, 4210752);
		
		// Draw information text	 
		this.RFTick.text = "" + TurretUtil.getRFUsePerTick(turret.getBaseEnergyPerTick(), this.sliderRange.getValue());
		this.RFShot.text = "" + TurretUtil.getRFUsePerShot(turret.getBaseEnergyPerShot(), this.sliderDamage.getValue(), this.sliderAccuracy.getValue(), this.sliderCooldown.getValue());
		
		// Tooltips
		List<String> tip = new ArrayList<String>();
		tip.add("ToolTip");
		this.sliderAccuracy.addTooltip(tip);
	
	}
	
	@Override
	protected void actionPerformed(GuiButton guibutton) {
		if (guibutton.id == 0) {
			sendUpdateMessage();
		}
	}
	
	private void sendUpdateMessage() {
		UpdateTurretMessage message = new UpdateTurretMessage(turret.xCoord, turret.yCoord, turret.zCoord, this.buttonToggleTarget.getSelectedIndex(), this.sliderDamage.getValue(), this.sliderAccuracy.getValue(), this.sliderCooldown.getValue(), this.sliderRange.getValue());
		GekosLasers.network.sendToServer(message);
	}
}
