package com.gekox.gekosLasers.client.gui;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.client.gui.slot.LaserGunSlot;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class TurretContainerTier1 extends Container {

	protected TileEntityTurret turret;
	
	public TurretContainerTier1(InventoryPlayer player, TileEntityTurret turret) {
		
		this.turret = turret;
		
		// Construct and add slots to the container
		
		// Turret inventory
		this.addSlotToContainer(new LaserGunSlot(turret, 0, 26, 45));
		
		// Player inventory
		for(int x = 0; x < 9; x++) {
			this.addSlotToContainer(new Slot(player, x, 33 + x * 18, 177));
		}
		
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 9; x++) {
				this.addSlotToContainer(new Slot(player, 9 + x + y * 9, 33 + x * 18, 119 + y * 18));
			}
		}
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return turret.isUseableByPlayer(player);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
		ItemStack stack = null;
		Slot slotObject = (Slot) inventorySlots.get(slot);
		
		// null checks and checks if the item can be stacked (maxStackSize > 1)
		if (slotObject != null && slotObject.getHasStack()) {
			ItemStack stackInSlot = slotObject.getStack();
			stack = stackInSlot.copy();
			
			if (stackInSlot.stackSize <= 0) {
				slotObject.putStack(null);
			} 
			
			else {
				slotObject.onSlotChanged();
			}
			
			if (stackInSlot.stackSize == stack.stackSize) {
				return null;
			}
			
			slotObject.onPickupFromSlot(player, stackInSlot);
			
		}
		
		return stack;
	}

}
