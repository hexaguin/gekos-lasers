package com.gekox.gekosLasers.client.handler;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.client.gui.TurretContainerTier1;
import com.gekox.gekosLasers.client.gui.TurretGuiBase;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GUIHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

		TileEntity te = world.getTileEntity(x, y, z);
		
		if(!(te instanceof TileEntityTurret)) {
			return null;
		}
		
		switch(ID) {
			case 0:
				return new TurretContainerTier1(player.inventory, (TileEntityTurret)te);
			default:
				return null;
			
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		
		TileEntity te = world.getTileEntity(x, y, z);
		
		if(!(te instanceof TileEntityTurret)) {
			return null;
		}
		
		switch(ID) {
			case 0:
				return new TurretGuiBase(player.inventory, (TileEntityTurret)te);
			default:
				return null;
			
		}
	}

}
