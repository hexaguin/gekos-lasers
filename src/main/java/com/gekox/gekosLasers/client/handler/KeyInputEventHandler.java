package com.gekox.gekosLasers.client.handler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.client.settings.KeyBindings;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.network.UpdateLaserSizeAndDepth;
import com.gekox.gekosLasers.reference.Key;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;

// Listens for keys pressed
public class KeyInputEventHandler {

	// Returns the key that is pressed
	private static Key getKeyBindingPressed() {
		
		if(KeyBindings.laserSize.isPressed()) {
			return Key.LASER_SIZE;
		}
		
		else if(KeyBindings.laserDepth.isPressed()) {
			return Key.LASER_DEPTH;
		}
		
		return Key.UNKNOWN;
		
	}
	
	@SubscribeEvent
	public void handleKeyInputEvent(InputEvent.KeyInputEvent e) {
		
		Key key = getKeyBindingPressed();
		if(key == Key.UNKNOWN) return;
		
		System.out.println(key);
		System.out.println(key.ordinal());
		
		if (FMLClientHandler.instance().getClient().inGameHasFocus) {
			
			if (FMLClientHandler.instance().getClientPlayerEntity() != null) {
				
				EntityPlayer entityPlayer = FMLClientHandler.instance().getClientPlayerEntity();

				if (entityPlayer.getCurrentEquippedItem() != null) {
					
					ItemStack currentlyEquippedItemStack = entityPlayer.getCurrentEquippedItem();

					if (currentlyEquippedItemStack.getItem() instanceof ItemLaserGun) {
						
						if (entityPlayer.worldObj.isRemote) {
							GekosLasers.network.sendToServer(new UpdateLaserSizeAndDepth(key));
						}
						
						else {
							if(key == Key.LASER_DEPTH)
								((ItemLaserGun) currentlyEquippedItemStack.getItem()).changeNumOfThingsBroken(currentlyEquippedItemStack);
							else if(key == Key.LASER_SIZE)
								((ItemLaserGun) currentlyEquippedItemStack.getItem()).changeBlockBreakRadius(currentlyEquippedItemStack);
						}
					}
				}
			}
		}
		
		
	}
	
}
