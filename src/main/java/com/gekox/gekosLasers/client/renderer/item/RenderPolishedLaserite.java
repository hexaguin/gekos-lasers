package com.gekox.gekosLasers.client.renderer.item;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

/**
 * Polished laserite renderer
 * 
 * @author Xavier Hunt
 *
 */
public class RenderPolishedLaserite extends ItemRenderer implements IItemRenderer {

	public RenderPolishedLaserite(Minecraft p_i1247_1_) {
		super(p_i1247_1_);
		// TODO Auto-generated constructor stub
	}
	
	public RenderPolishedLaserite() {
		this(Minecraft.getMinecraft());
	}

	private static final ResourceLocation RES_ITEM_GLINT = new ResourceLocation("textures/misc/enchanted_item_glint.png");
	private static RenderItem renderItem = new RenderItem();
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return (type == ItemRenderType.INVENTORY);// || (type == ItemRenderType.EQUIPPED_FIRST_PERSON);
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack stack, Object... data) {
		
		// Get icon index for the texture
		IIcon icon = stack.getIconIndex();
		// Use vanilla code to render the icon in a 16x16 square of inventory slot
		renderItem.renderIcon(0, 0, icon, 16, 16);
		
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		
		// ====================== Render OpenGL square shape ======================
		
//		GL11.glPushMatrix();
//		
//		GL11.glEnable(GL11.GL_BLEND);
//		GL11.glDepthMask(false);
//		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
//		
//		Tessellator tessellator = Tessellator.instance;
//		tessellator.startDrawingQuads();
//		
//		Minecraft.getMinecraft().renderEngine.bindTexture(RES_ITEM_GLINT);
//		
//		float f9 = (float)(Minecraft.getSystemTime() % 3000L) / 3000.0F * 8.0F;
//		GL11.glTranslatef(f9, 0.0F, 0.0F);
//		
//		tessellator.setColorRGBA(255, 255, 255, 128);
//		tessellator.addVertexWithUV(0, 0, 0, 0, 0);
//		tessellator.addVertexWithUV(0, 16, 0, 0, 1);
//		tessellator.addVertexWithUV(16, 16, 0, 1, 1);
//		tessellator.addVertexWithUV(16, 0, 0, 1, 0);
//		tessellator.draw();
//		
//		GL11.glDepthMask(true);
//		GL11.glDisable(GL11.GL_BLEND);
//		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
//		
//		GL11.glPopMatrix();
		
		TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
        Item item = stack.getItem();
        Block block = Block.getBlockFromItem(item);
        Tessellator tessellator = Tessellator.instance;
		
		GL11.glDepthFunc(GL11.GL_EQUAL);
        GL11.glDisable(GL11.GL_LIGHTING);
        texturemanager.bindTexture(RES_ITEM_GLINT);
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(768, 1, 1, 0);
        float f7 = 0.76F;
        GL11.glColor4f(0.5F * f7, 0.25F * f7, 0.8F * f7, 1.0F);
        GL11.glMatrixMode(GL11.GL_TEXTURE);
        GL11.glPushMatrix();
        float f8 = 0.125F;
        GL11.glScalef(f8, f8, f8);
        float f9 = (float)(Minecraft.getSystemTime() % 3000L) / 3000.0F * 8.0F;
        GL11.glTranslatef(f9, 0.0F, 0.0F);
        GL11.glRotatef(-50.0F, 0.0F, 0.0F, 1.0F);
        renderItemIn2D(tessellator, 0.0F, 0.0F, 1.0F, 1.0F, 256, 256, 0.0625F);
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        GL11.glScalef(f8, f8, f8);
        f9 = (float)(Minecraft.getSystemTime() % 4873L) / 4873.0F * 8.0F;
        GL11.glTranslatef(-f9, 0.0F, 0.0F);
        GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
        renderItemIn2D(tessellator, 0.0F, 0.0F, 1.0F, 1.0F, 256, 256, 0.0625F);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glDepthFunc(GL11.GL_LEQUAL);
		
	}

}
