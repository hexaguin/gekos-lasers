package com.gekox.gekosLasers.client.renderer.tileEntity;

import org.lwjgl.opengl.GL11;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurretSphere;
import com.gekox.gekosLasers.client.render.model.ModelTurret;
import com.gekox.gekosLasers.client.render.model.ModelTurretSphere;
import com.gekox.gekosLasers.reference.Textures;
import com.gekox.gekosLasers.utility.Maths;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityRendererTurretSphere extends TileEntitySpecialRenderer {

	private final ModelTurretSphere modelTurretSphere = new ModelTurretSphere();
	private final RenderItem customRenderItem;
	
	public TileEntityRendererTurretSphere() {
		
		customRenderItem = new RenderItem() {
			@Override
			public boolean shouldBob() {
				return false;
			}
		};

		customRenderItem.setRenderManager(RenderManager.instance);
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick) {
		if (tileEntity instanceof TileEntityTurretSphere) {
			
			TileEntityTurretSphere tileEntityTurretSphere = (TileEntityTurretSphere) tileEntity;

			GL11.glPushMatrix();
			
			// Scale, Translate, Rotate
			GL11.glTranslated(x, y, z);

			
			// Cheers to vazkii for the bobbing method!
			// https://github.com/Vazkii/Botania/blob/master/src/main/java/vazkii/botania/client/render/tile/RenderTileFloatingFlower.java
			
			double time = tileEntity.getWorldObj().getTotalWorldTime();
			GL11.glTranslatef(0, 0.1f * (float)Math.sin(time * 0.05f), 0);
			

			// Bind texture
			this.bindTexture(Textures.Model.TURRET_SPHERE);

			// Render
			modelTurretSphere.render();

			GL11.glPopMatrix();
		}
	}
}
