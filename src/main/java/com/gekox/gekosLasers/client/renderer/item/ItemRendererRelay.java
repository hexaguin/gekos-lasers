package com.gekox.gekosLasers.client.renderer.item;

import org.lwjgl.opengl.GL11;

import cofh.repack.codechicken.lib.render.TextureSpecial;

import com.gekox.gekosLasers.client.render.model.ModelRelay;
import com.gekox.gekosLasers.client.render.model.ModelTurret;
import com.gekox.gekosLasers.reference.Textures;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ItemRendererRelay implements IItemRenderer {

	private final ModelRelay modelRelay;
	
	public ItemRendererRelay() {
		modelRelay = new ModelRelay();
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		switch (type)
		{
			case ENTITY:
			{
				renderTurret(-0.5F, -0.38F, 0.5F);
				return;
			}
			case EQUIPPED:
			{
				renderTurret(0.0F, 0.0F, 1.0F);
				return;
			}
			case EQUIPPED_FIRST_PERSON:
			{
				renderTurret(0.0F, 0.0F, 1.0F);
				return;
			}
			case INVENTORY:
			{
				renderTurret(-1.0F, -0.9F, 0.0F);
				return;
			}
			default:
		}
		
	}

	private void renderTurret(float x, float y, float z) {
		
		GL11.glPushMatrix();
		
		GL11.glScalef(1, 1, 1);
		GL11.glTranslatef(x, y, z);
		GL11.glRotatef(-90f, 1, 0, 0);
		
		// Binds the texture
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(Textures.Model.RELAY);
		
		modelRelay.render();
		
		GL11.glPopMatrix();
		
	}

}
