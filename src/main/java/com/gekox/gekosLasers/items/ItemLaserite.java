package com.gekox.gekosLasers.items;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Laserite item
 * 
 * @author Xav
 *
 */

public class ItemLaserite extends ItemGL {

	// Constructor
	public ItemLaserite() {
		
		super();
		this.setUnlocalizedName(Names.Items.LASERITE);
		
	}
}
