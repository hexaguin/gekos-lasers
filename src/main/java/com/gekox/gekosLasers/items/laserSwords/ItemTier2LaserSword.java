package com.gekox.gekosLasers.items.laserSwords;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.items.ItemLaserSword;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTier2LaserSword extends ItemLaserSword {

	private static String unLocName = Names.Items.LASER_UPGRADE_NAMES[1] + "_" + Names.Items.LASER_SWORD_BASENAME;
	
	// Constructor
	public ItemTier2LaserSword(ItemStack stack) {
		super(stack);
		this.setUnlocalizedName(unLocName);
		
		this.damage *= ConfigSettings.TIER_2_BASE_DAMAGE;
		this.energyPerUse = ConfigSettings.TIER_2_BASE_ENERGY_USE;
	}
	
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		list.add(LaserReference.FLAVOR_SWORD);
		super.addInformation(stack, player, list, b0);
	}
}
