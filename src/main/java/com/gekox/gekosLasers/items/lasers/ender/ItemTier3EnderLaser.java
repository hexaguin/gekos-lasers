package com.gekox.gekosLasers.items.lasers.ender;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.items.lasers.ItemTier3Laser;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTier3EnderLaser extends ItemTier3Laser {
	
	private static String unLocName = Names.Items.LASER_UPGRADE_NAMES[2] + "_" + Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[4];
	
	// Constructor
	public ItemTier3EnderLaser() {
		this(new ItemStack(ModItems.itemTier3LaserCasing, 1, 4));
	}
	
	public ItemTier3EnderLaser(ItemStack container) {
		
		super(container, unLocName);
		
		this.setLaserDoTeleport(true);

		this.setLaserDamage((int) (this.getLaserDamage() * ConfigSettings.ENDER_DAMAGE_MODIFER));
		this.setCoolDown((int) (this.getCoolDown() * ConfigSettings.ENDER_COOLDOWN_MODIFER));
		this.energyPerUse *= ConfigSettings.ENDER_ENERGY_MODIFER;
	}
	
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		list.add(LaserReference.FLAVOR_ENDER);
		super.addInformation(stack, player, list, b0);
	}
}
