package com.gekox.gekosLasers.items.lasers.wide;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.items.ItemLaserGun;
import com.gekox.gekosLasers.items.lasers.ItemTier4Laser;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Names;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTier4WideMiningLaser extends ItemTier4Laser {

	private static String unLocName = Names.Items.LASER_UPGRADE_NAMES[3] + "_" + Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[2];
	
	// Constructor
	public ItemTier4WideMiningLaser() {
		this(new ItemStack(ModItems.itemTier4LaserCasing, 1, 2));
	}
	
	public ItemTier4WideMiningLaser(ItemStack container) {
		
		super(container, unLocName);

		this.setLaserBlockBreakRadius(5);
		this.setLaserStrength(20);
		
		this.setLaserDamage((int) (this.getLaserDamage() * ConfigSettings.WIDE_DAMAGE_MODIFER));
		this.setCoolDown((int) (this.getCoolDown() * ConfigSettings.WIDE_COOLDOWN_MODIFER));
		this.energyPerUse *= ConfigSettings.WIDE_ENERGY_MODIFER;
		
	}
	
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		list.add(LaserReference.FLAVOR_WIDE);
		super.addInformation(stack, player, list, b0);
	}
	
}
