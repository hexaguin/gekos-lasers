package com.gekox.gekosLasers.items;

import com.gekox.gekosLasers.GekosLasers;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemEnergizedLaserite extends ItemGL {

	public ItemEnergizedLaserite() {
		
		super();
		this.setUnlocalizedName(Names.Items.ENERGIZED_LASERITE);
		
	}
	
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1ItemStack, int i) {
		return true;
	}
}
