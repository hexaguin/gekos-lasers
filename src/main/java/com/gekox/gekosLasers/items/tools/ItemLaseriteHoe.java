package com.gekox.gekosLasers.items.tools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.tools.LaseriteTools;

//Laserite Hoe
//
//This is made from laserite crystals, and is brittle


public class ItemLaseriteHoe extends ItemHoeGL {

	public ItemLaseriteHoe(ToolMaterial material) {
		super(material);
		this.setUnlocalizedName(Names.Items.LASERITE_HOE);
	}
	
	public ItemLaseriteHoe() {
		this(LaseriteTools.LASERITE_TOOL_MATERIAL);
	}
	
	// Repairable
	@Override
	public boolean getIsRepairable(ItemStack item1, ItemStack item2) {
		
		if(item1.getItem() == ModItems.itemLaseriteHoe) {
			return (item2.getItem() == ModItems.itemLaserite) ? true : super.getIsRepairable(item1, item2);
		}
		
		return false;
		
	}

}