package com.gekox.gekosLasers.items.tools;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.tools.LaseriteTools;

//Laserite Pickaxe
//
//This is made from laserite crystals, and is brittle


public class ItemLaseritePickaxe extends ItemPickaxeGL {

	public ItemLaseritePickaxe(ToolMaterial material) {
		super(material);
		this.setUnlocalizedName(Names.Items.LASERITE_PICKAXE);
	}
	
	public ItemLaseritePickaxe() {
		this(LaseriteTools.LASERITE_TOOL_MATERIAL);
	}
	
	// Repairable
	@Override
	public boolean getIsRepairable(ItemStack item1, ItemStack item2) {
		
		if(item1.getItem() == ModItems.itemLaseritePickaxe) {
			return (item2.getItem() == ModItems.itemLaserite) ? true : super.getIsRepairable(item1, item2);
		}
		
		return false;
		
	}

}