package com.gekox.gekosLasers.items;

import java.util.List;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import com.gekox.gekosLasers.entity.projectile.laser.*;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.Color;
import com.gekox.gekosLasers.utility.ColorUtils;
import com.gekox.gekosLasers.utility.LaserUtils;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;
import com.gekox.gekosLasers.utility.StringUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/** Laser thing base class. <br>
 *  A laser thing is something that shoots lasers
 *  
 *  <p>
 *  
 * @author Xavier Hunt
 *
 */

public class ItemLaserGun extends ItemLaserThing {

	/* Variables */
	
	// Laser properties
	private int laserDamage = 0;
	private int laserStrength = -1;
	private int laserBlockBreakRadius = 1;
	private float laserExplosionSize = 0;
	private boolean laserDoExplosionDamage = false;
	private boolean laserDoFireDamage = false;
	private boolean laserDoTeleport = false;
	private int laserBurnTime = 0;
	
	private ProjectileLaser laser;
	
	private int numOfThingsBroken;
	
	private int currentBlockBreakRadius = 1;
	private int currentNumOfThingsBroken = 0;
	
	private String DEPTH_TAG = "CurrentDepth";
	private String RADIUS_TAG = "CurrentRadius";
	
	/* Constructor */
	
	public ItemLaserGun() {
		super();	
	}
	
	public ItemLaserGun(ProjectileLaser laser) {
		this.laser = laser;
	}
	
	/* Update */

	
	/* Do things */
	
	
	
	/**
	 * Sets the laser projectile for this laser thing
	 * @param laser
	 */
	public ItemLaserGun setLaser(ProjectileLaser laser) {
		this.laser = laser;
		return this;
	}
	
	/**
	 * Gets the laser projectile associated with this laser item
	 * @param stack
	 * @return The laser projectile
	 */
	public ProjectileLaser getLaser(ItemStack stack) {
		ItemLaserGun laserThing = (ItemLaserGun)getLaserThing(stack);
		return (laserThing == null) ? null : laserThing.laser;
	}
	
	public int getLaserDamage() {
		return laserDamage;
	}

	public void setLaserDamage(int laserDamage) {
		this.laserDamage = laserDamage;
	}

	public int getLaserStrength() {
		return laserStrength;
	}

	public void setLaserStrength(int laserStrength) {
		this.laserStrength = laserStrength;
	}

	public int getLaserBlockBreakRadius() {
		return laserBlockBreakRadius;
	}

	public void setLaserBlockBreakRadius(int laserBlockBreakRadius) {
		this.laserBlockBreakRadius = laserBlockBreakRadius;
		this.currentBlockBreakRadius = laserBlockBreakRadius;
	}

	public int getNumOfThingsBroken() {
		return numOfThingsBroken;
	}

	public void setNumOfThingsBroken(int numOfThingsBroken) {
		this.numOfThingsBroken = numOfThingsBroken;
		this.currentNumOfThingsBroken = numOfThingsBroken;
	}

	public int getCurrentNumOfThingsBroken() {
		return this.currentNumOfThingsBroken;
	}

	public int getCurrentBlockBreakRadius() {
		return currentBlockBreakRadius;
	}

	public boolean isLaserDoExplosionDamage() {
		return laserDoExplosionDamage;
	}

	public void setLaserDoExplosionDamage(boolean laserDoExplosionDamage) {
		this.laserDoExplosionDamage = laserDoExplosionDamage;
	}

	public float getLaserExplosionSize() {
		return laserExplosionSize;
	}

	public void setLaserExplosionSize(float laserExplosionSize) {
		this.laserExplosionSize = laserExplosionSize;
	}

	public boolean isLaserDoFireDamage() {
		return laserDoFireDamage;
	}

	public void setLaserDoFireDamage(boolean laserDoFireDamage) {
		this.laserDoFireDamage = laserDoFireDamage;
	}

	public int getLaserBurnTime() {
		return laserBurnTime;
	}

	public void setLaserBurnTime(int laserBurnTime) {
		this.laserBurnTime = laserBurnTime;
	}

	public boolean isLaserDoTeleport() {
		return laserDoTeleport;
	}

	public void setLaserDoTeleport(boolean laserDoTeleport) {
		this.laserDoTeleport = laserDoTeleport;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		
		Color color = Color.color[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)];
		
		this.setLaser(LaserUtils.determineLaser(world, player, color)
				.setupLaser(this.getLaserDamage(), 
							this.getLaserStrength(), 
							NBTHelper.getInt(stack, RADIUS_TAG),
							this.isLaserDoExplosionDamage(), 
							this.getLaserExplosionSize(), 
							this.isLaserDoFireDamage(),
							this.getLaserBurnTime(),
							this.isLaserDoTeleport()));
		
		this.laser.setColor(color);
		
		if(!world.isRemote) {
			//System.out.println(canFire(stack));
			
			// Check energy levels
			if(canFire(stack, world)) {
				shoot(stack, player, this.laser);
			}
		}
		
		return stack;
		
	}
	
	@Override
	public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_, World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {
        return false;
    }

	public void shoot(ItemStack stack, EntityPlayer player, ProjectileLaser laser) {
		
		//LogHelper.info("Shooting");
		
		if(!player.capabilities.isCreativeMode)
		{
			useEnergy(stack, false);
		}
		
		laser.numOfThingsBroken = NBTHelper.getInt(stack, DEPTH_TAG);
		
		this.setNextShot(stack, player.worldObj);
		
		World world = player.worldObj;
		world.spawnEntityInWorld(laser);
		world.playSoundAtEntity(player, Reference.RESOURCE_PREFIX + "laser.pew", 0.5f, 1);
		
	}
	
	public void shoot(ItemStack stack, ProjectileLaser laser, World world, int x, int y, int z) {
		LogHelper.debug("Shooting: " + stack.toString());

		useEnergy(stack, false);
		
		laser.numOfThingsBroken = NBTHelper.getInt(stack, DEPTH_TAG);
		world.spawnEntityInWorld(laser);
		
		world.playSoundEffect(x, y, z, Reference.RESOURCE_PREFIX + "laser.pew", 0.5f, 1);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldRotateAroundWhenRendering() {
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b0) {
		
		String color = StringUtils.toTitleCase(ColorUtils.names[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)]);
		String code = ColorUtils.codes[NBTHelper.getInt(stack, LaserReference.LASER_COLOR_TAG)];
		list.add(String.format("%s%s %slaser", code, color, ColorUtils.CODE_LIGHT_GRAY));
		
		int rad = (this.laserBlockBreakRadius - 1) * 2 + 1;
		int currentRad = (NBTHelper.getInt(stack, RADIUS_TAG) - 1) * 2 + 1;
		
		if(GuiScreen.isShiftKeyDown()) {
			list.add(String.format("Energy: %d/%dRF", this.getEnergyStored(stack), this.getMaxEnergyStored(stack)));
			list.add(String.format("Uses left: %d", this.getEnergyStored(stack) / this.energyPerUse));
			list.add(String.format("Maximum area of effect: %dx%dx%d", rad, rad, this.numOfThingsBroken));
			list.add(String.format("Current area of effect: %dx%dx%d", currentRad, currentRad, NBTHelper.getInt(stack, DEPTH_TAG)));
		}
		
		else {
			list.add(String.format("%sPress %sSHIFT%s for details", ColorUtils.CODE_LIGHT_GRAY, ColorUtils.CODE_YELLOW, ColorUtils.CODE_LIGHT_GRAY));
		}
	}
	
	// Change the laser strength and block break radius
	
	public void changeNumOfThingsBroken (ItemStack stack) {
		
		if(!(stack.getItem() instanceof ItemLaserGun))
			return;
		
		ItemLaserGun gun = (ItemLaserGun) stack.getItem();
		
		//gun.currentNumOfThingsBroken++;
		NBTHelper.setInteger(stack, DEPTH_TAG, NBTHelper.getInt(stack, DEPTH_TAG) + 1);
		
		if(NBTHelper.getInt(stack, DEPTH_TAG) > gun.numOfThingsBroken)
			NBTHelper.setInteger(stack, DEPTH_TAG, 0);
		
	}
	
	public void changeBlockBreakRadius (ItemStack stack) {
		
		if(!(stack.getItem() instanceof ItemLaserGun))
			return;
		
		ItemLaserGun gun = (ItemLaserGun) stack.getItem();
		
		NBTHelper.setInteger(stack, RADIUS_TAG, NBTHelper.getInt(stack, RADIUS_TAG) + 1);
		
		if(NBTHelper.getInt(stack, RADIUS_TAG) > gun.laserBlockBreakRadius)
			NBTHelper.setInteger(stack, RADIUS_TAG, 0);
		
	}
}
