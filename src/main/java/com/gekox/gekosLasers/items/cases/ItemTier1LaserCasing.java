package com.gekox.gekosLasers.items.cases;

import com.gekox.gekosLasers.items.MetaItemGL;
import com.gekox.gekosLasers.reference.Names;

/**
 * Casings for normal lasers
 * 
 * @author X.Hunt
 *
 */

public class ItemTier1LaserCasing extends MetaItemGL {

	public ItemTier1LaserCasing() {
		super(Names.Items.LASER_TYPES.length);
		this.setUnlocalizedName(Names.Items.LASER_UPGRADE_NAMES[0] + "_" + Names.Items.CASE_BASENAME);
		
		for(int i = 0; i < this.getNumberOfItems(); i++) {
			this.names[i] = Names.Items.LASER_TYPES[i];
		}
	}
}
