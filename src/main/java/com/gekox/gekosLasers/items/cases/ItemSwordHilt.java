package com.gekox.gekosLasers.items.cases;

import com.gekox.gekosLasers.items.MetaItemGL;
import com.gekox.gekosLasers.reference.Names;

public class ItemSwordHilt extends MetaItemGL {

	public ItemSwordHilt() {
		
		super(Names.Items.LASER_UPGRADE_NAMES.length);
		
		this.setUnlocalizedName(Names.Items.HILT_BASENAME);
		
		for(int i = 0; i < this.getNumberOfItems(); i++) {
			this.names[i] = Names.Items.LASER_UPGRADE_NAMES[i];
		}
	}
	
}
