package com.gekox.gekosLasers.items.cases;

import net.minecraft.item.ItemStack;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.items.ItemGL;
import com.gekox.gekosLasers.items.MetaItemGL;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.utility.Color;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


/**
 * Casing for laser things
 * 
 * @author Xavier Hunt
 *
 */

//public class ItemLaserCasingExplosive extends MetaItemGL {
//	
//	public ItemLaserCasingExplosive() {
//		super(2);
//		this.setUnlocalizedName(Names.Items.LASER_CASING_EXPLOSIVE);
//
//		for(int i = 0; i < this.getNumberOfItems(); i++) {
//			this.names[i] = Names.Items.LASER_UPGRADE_NAMES[i];
//		}
//	}
//	
//	@SideOnly(Side.CLIENT)
//	public boolean hasEffect(ItemStack stack, int i) {
//		int meta = stack.getItemDamage();
//		return meta == 1 ? true : false;
//	}
//}
