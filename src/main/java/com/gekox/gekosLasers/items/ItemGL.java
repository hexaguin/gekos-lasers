package com.gekox.gekosLasers.items;

import com.gekox.gekosLasers.creativeTab.CreativeTabGL;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Wrapper class for all items.
 * @author Xav
 *
 */
// Overrides from
// https://github.com/pahimar/LetsModReboot/blob/master/src/main/java/com/pahimar/letsmodreboot/item/ItemLMRB.java


public class ItemGL extends Item {

	public ItemGL() {
		
		super();
		this.setCreativeTab(CreativeTabGL.GL_TAB);
		
	}
	
	/**
	 * Gets the unlocalized name
	 */
	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}
	
	/**
	 * Gets the unlocalized name
	 * 
	 * @param itemStack
	 */
	@Override
    public String getUnlocalizedName(ItemStack itemStack)
    {
        return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }
	
	/**
	 * Registers the item's icon
	 */
	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
		itemIcon = iconRegister.registerIcon(this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
    }
	
	/**
	 * Returns the unlocalized name, in an unwrapped form
	 * @param unlocalizedName
	 * @return
	 */
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
}
