package com.gekox.gekosLasers.network;

import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;

import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class UpdateEnergyMessage implements IMessage, IMessageHandler<UpdateEnergyMessage, IMessage> {

	private int x, y, z, energy;
	
	public UpdateEnergyMessage() {
		
	}
	
	public UpdateEnergyMessage(int x, int y, int z, int e) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.energy = e;
		
	}
	
	@Override
	public IMessage onMessage(UpdateEnergyMessage message, MessageContext ctx) {
		
//		System.out.println("Recieved energy update message:");
//		System.out.println(message.getX() + " " + message.getY() + " " + message.getZ() + " " + message.getEnergy());
		
		World world = Minecraft.getMinecraft().thePlayer.worldObj;
		TileEntityTurret turret = (TileEntityTurret)world.getTileEntity(message.getX(), message.getY(), message.getZ());
		
		if(turret != null) {
			turret.setEnergyStored(message.getEnergy());
		}
		
		return null;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.energy = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.x);
		buf.writeInt(this.y);
		buf.writeInt(this.z);
		buf.writeInt(this.energy);
	}
	
	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getZ() {
		return this.z;
	}

	public int getEnergy() {
		return this.energy;
	}

}
