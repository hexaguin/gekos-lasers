package com.gekox.gekosLasers.utility;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class BlockUtils {

	public static void breakBlock(World world, int x, int y, int z) {
		
		if(world.isRemote) {
		
			if(world.getBlock(x, y, z) != Blocks.bedrock) {
				
				if(world.setBlockToAir(x, y, z)) {
					System.out.println(String.format("Breaking block at: %d, %d, %d", x, y, z));
					dropBlock(world, x, y, z);
				}
			}
		}
	}
	
	public static void dropBlock(World world, int x, int y, int z) {
		if(!world.isRemote) {
			
			if(world.getBlock(x, y, z) != Blocks.bedrock) {
				System.out.println(String.format("Dropping block at: %d, %d, %d", x, y, z));
				world.getBlock(x, y, z).dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
			}
		}
	}
}
