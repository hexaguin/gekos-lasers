package com.gekox.gekosLasers.utility;

import java.util.List;

import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

// HUD Interface

public interface IHUDInfo {
	
	@SideOnly(Side.CLIENT)
	public void addHudInfo(ItemStack statck, List<String> list);
	
}
