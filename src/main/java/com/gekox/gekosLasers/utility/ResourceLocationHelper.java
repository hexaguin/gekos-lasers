package com.gekox.gekosLasers.utility;

import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.util.ResourceLocation;

// Pahimar's
// https://github.com/pahimar/Equivalent-Exchange-3/blob/master/src/main/java/com/pahimar/ee3/util/ResourceLocationHelper.java

public class ResourceLocationHelper
{
    public static ResourceLocation getResourceLocation(String modId, String path)
    {
        return new ResourceLocation(modId, path);
    }

    public static ResourceLocation getResourceLocation(String path)
    {
        return getResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, path);
    }
}