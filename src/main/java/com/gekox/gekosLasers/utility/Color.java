package com.gekox.gekosLasers.utility;

// Basic color class.
// Just holds some data for the color for now (r, g, b, a

public enum Color {

	
	WHITE(221, 221, 221),
	ORANGE(219, 125, 62),
	MAGENTA(179, 80, 188),
	LIGHT_BLUE(107, 138, 201),
	YELLOW(177, 166, 39),
	LIME(65, 174, 56),
	PINK(208, 132, 153),
	GRAY(64, 64, 64),
	LIGHT_GRAY(154, 161, 161),
	CYAN(46, 110, 137),
	PURPLE(126, 61, 181),
	BLUE(46, 56, 141),
	BROWN(79, 50, 31),
	GREEN(53, 70, 27),
	RED(150, 52, 48),
	BLACK(25, 22, 22);
	
	public int r;
	public int g;
	public int b;
	public int a;
	
	public static final Color[] color = {WHITE, ORANGE, MAGENTA, LIGHT_BLUE, YELLOW, LIME, PINK, GRAY, LIGHT_GRAY, CYAN, PURPLE, BLUE, BROWN, GREEN, RED, BLACK};

	private Color(int r, int g, int b, int a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	private Color(int r, int g, int b) {
		this(r, g, b, 255);
	}
	
	private Color() {
		this(0, 0, 0);
	}
	
	public String toString() {
		return String.format("(%d, %d, %d, %d)", r, g, b, a);
	}
}
