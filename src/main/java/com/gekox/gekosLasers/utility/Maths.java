package com.gekox.gekosLasers.utility;

public class Maths {

	// Standard lerp function
	public static float lerp(float a, float b, float t) {
		return a + t * (b - a);
	}
	
}
