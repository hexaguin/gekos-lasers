package com.gekox.gekosLasers.utility;

import net.minecraft.entity.Entity;
import net.minecraft.util.EntityDamageSourceIndirect;

import com.gekox.gekosLasers.entity.projectile.laser.ProjectileLaser;
import com.gekox.gekosLasers.reference.Reference;

public class LaserDamageSource extends EntityDamageSourceIndirect {

	public ProjectileLaser laser;
	
	public LaserDamageSource(Entity laser, Entity entity) {
		super(Reference.LASER_DAMAGE_SOURCE_NAME, laser, entity);
		this.setDamageIsAbsolute();
		
		this.laser = (ProjectileLaser) laser;
	}
	
}
