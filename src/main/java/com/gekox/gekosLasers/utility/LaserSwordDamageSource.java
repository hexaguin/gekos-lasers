package com.gekox.gekosLasers.utility;

import net.minecraft.entity.Entity;
import net.minecraft.util.EntityDamageSource;

import com.gekox.gekosLasers.reference.Reference;

public class LaserSwordDamageSource extends EntityDamageSource {

	public LaserSwordDamageSource(Entity entity) {
		super(Reference.SWORD_DAMAGE_SOURCE_NAME, entity);
	}
	
}
