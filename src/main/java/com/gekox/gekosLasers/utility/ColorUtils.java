package com.gekox.gekosLasers.utility;

public class ColorUtils {

	public static final String[] names = {"white",
		   "orange",
		   "magenta",
		   "lightBlue",
		   "yellow",
		   "lime",
		   "pink",
		   "gray",
		   "lightGray",
		   "cyan",
		   "purple",
		   "blue",
		   "brown",
		   "green",
		   "red",
		   "black"};

	public static final String CODE_BLACK = (char) 167 + "0";
	public static final String CODE_BLUE = (char) 167 + "1";
	public static final String CODE_GREEN = (char) 167 + "2";
	public static final String CODE_CYAN = (char) 167 + "3";
	public static final String CODE_RED = (char) 167 + "4";
	public static final String CODE_PURPLE = (char) 167 + "5";
	public static final String CODE_ORANGE = (char) 167 + "6";
	public static final String CODE_LIGHT_GRAY = (char) 167 + "7";
	public static final String CODE_GRAY = (char) 167 + "8";
	public static final String CODE_LIGHT_BLUE = (char) 167 + "9";
	public static final String CODE_LIME = (char) 167 + "a";
	public static final String CODE_BRIGHT_BLUE = (char) 167 + "b";
	public static final String CODE_PINK = (char) 167 + "c";
	public static final String CODE_MAGENTA = (char) 167 + "d";
	public static final String CODE_YELLOW = (char) 167 + "e";
	public static final String CODE_WHITE = (char) 167 + "f";
	public static final String CODE_OBFUSCATED = (char) 167 + "k";
	public static final String CODE_CODE_BOLD = (char) 167 + "l";
	public static final String CODE_STRIKETHROUGH = (char) 167 + "m";
	public static final String CODE_UNDERLINE = (char) 167 + "n";
	public static final String CODE_ITALIC = (char) 167 + "o";
	public static final String CODE_END = (char) 167 + "r";
	
	public static final String[] codes = {CODE_WHITE,
			  CODE_ORANGE,
			  CODE_MAGENTA,
			  CODE_LIGHT_BLUE,
			  CODE_YELLOW,
			  CODE_LIME,
			  CODE_PINK,
			  CODE_GRAY,
			  CODE_LIGHT_GRAY,
			  CODE_CYAN,
			  CODE_PURPLE,
			  CODE_BLUE,
			  CODE_ORANGE,
			  CODE_GREEN,
			  CODE_RED,
			  CODE_GRAY};
		
}
