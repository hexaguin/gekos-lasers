package com.gekox.gekosLasers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cofh.mod.updater.IUpdatableMod;
import cofh.mod.updater.UpdateManager;

import com.gekox.gekosLasers.client.handler.GUIHandler;
import com.gekox.gekosLasers.client.handler.KeyInputEventHandler;
import com.gekox.gekosLasers.handler.ConfigHandler;
import com.gekox.gekosLasers.network.MessageTileEntityGL;
import com.gekox.gekosLasers.network.UpdateEnergyMessage;
import com.gekox.gekosLasers.network.UpdateLaserSizeAndDepth;
import com.gekox.gekosLasers.network.UpdateTurretMessage;
import com.gekox.gekosLasers.proxy.IProxy;
import com.gekox.gekosLasers.reference.ModReference;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION, guiFactory = Reference.GUI_FACTORY_CLASS, dependencies = Reference.DEPENDENCIES)
public class GekosLasers implements IUpdatableMod {

	// Main instance
	@Mod.Instance(Reference.MODID)
	public static GekosLasers instance;
	
	// Proxy
	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	public static IProxy proxy;
	
	// Logger
	public static final Logger log = LogManager.getLogger(Reference.MODID);
	
	// GUI Handler
	public GUIHandler GUI = new GUIHandler();
	
	// Neworking object
	public static SimpleNetworkWrapper network;
	
	// Pre Init
	// Loads configs, items, blocks...
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		
		LogHelper.info("Pre init starting");
		
		UpdateManager.registerUpdater(new UpdateManager(this, Reference.RELEASE_URL, Reference.DOWNLOAD_URL));
		
		ConfigHandler.init(e.getSuggestedConfigurationFile());
		FMLCommonHandler.instance().bus().register(new ConfigHandler());
		
		this.proxy.preInit(e);
		
		network = new SimpleNetworkWrapper(Reference.MODID);
		network.registerMessage(UpdateEnergyMessage.class, UpdateEnergyMessage.class, 0, Side.CLIENT);
		network.registerMessage(UpdateTurretMessage.class, UpdateTurretMessage.class, 1, Side.SERVER);
		network.registerMessage(UpdateLaserSizeAndDepth.class, UpdateLaserSizeAndDepth.class, 2, Side.SERVER);
		network.registerMessage(MessageTileEntityGL.class, MessageTileEntityGL.class, 3, Side.CLIENT);
		
		
		LogHelper.info("Pre init finished"); 
		
	}
	
	// Init
	// Setup GUI, tileEntites, rendering, event handlers, recipes, keybindings...
	@Mod.EventHandler
	public void init(FMLInitializationEvent e) {
		
		LogHelper.info("Init starting");
	
		NetworkRegistry.INSTANCE.registerGuiHandler(this, GUI);
		
		// Check other mods
		LogHelper.info("Checking if other mods are loaded");
		ModReference.MOD_THERMAL_FOUNDATION = Loader.isModLoaded("ThermalFoundation");
		ModReference.MOD_THERMAL_EXPANSION = Loader.isModLoaded("ThermalExpansion");
		
		ModReference.listMods();
		
		this.proxy.init(e);
		this.proxy.initRenderingAndTextures();
		
		LogHelper.info("Init finished");
	}
	
	// Post init
	// Runs after everything has initialized
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		
		LogHelper.info("Post init starting");
		
		this.proxy.postInit(e);
		
		LogHelper.info("Post init finished");
	}

	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	public String getModId() {
		return Reference.MODID;
	}

	@Override
	public String getModName() {
		return Reference.NAME;
	}

	@Override
	public String getModVersion() {
		return Reference.VERSION;
	}
	
}
