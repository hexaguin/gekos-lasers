package com.gekox.gekosLasers.init;

import com.gekox.gekosLasers.items.*;
import com.gekox.gekosLasers.items.cases.*;
import com.gekox.gekosLasers.items.laserSwords.*;
import com.gekox.gekosLasers.items.lasers.ender.ItemTier1EnderLaser;
import com.gekox.gekosLasers.items.lasers.ender.ItemTier2EnderLaser;
import com.gekox.gekosLasers.items.lasers.ender.ItemTier3EnderLaser;
import com.gekox.gekosLasers.items.lasers.ender.ItemTier4EnderLaser;
import com.gekox.gekosLasers.items.lasers.explosive.ItemTier1ExplosiveLaser;
import com.gekox.gekosLasers.items.lasers.explosive.ItemTier2ExplosiveLaser;
import com.gekox.gekosLasers.items.lasers.explosive.ItemTier3ExplosiveLaser;
import com.gekox.gekosLasers.items.lasers.explosive.ItemTier4ExplosiveLaser;
import com.gekox.gekosLasers.items.lasers.fire.ItemTier1FireLaser;
import com.gekox.gekosLasers.items.lasers.fire.ItemTier2FireLaser;
import com.gekox.gekosLasers.items.lasers.fire.ItemTier3FireLaser;
import com.gekox.gekosLasers.items.lasers.fire.ItemTier4FireLaser;
import com.gekox.gekosLasers.items.lasers.mining.ItemTier1MiningLaser;
import com.gekox.gekosLasers.items.lasers.mining.ItemTier2MiningLaser;
import com.gekox.gekosLasers.items.lasers.mining.ItemTier3MiningLaser;
import com.gekox.gekosLasers.items.lasers.mining.ItemTier4MiningLaser;
import com.gekox.gekosLasers.items.lasers.rifle.ItemTier1LaserRifle;
import com.gekox.gekosLasers.items.lasers.rifle.ItemTier2LaserRifle;
import com.gekox.gekosLasers.items.lasers.rifle.ItemTier3LaserRifle;
import com.gekox.gekosLasers.items.lasers.rifle.ItemTier4LaserRifle;
import com.gekox.gekosLasers.items.lasers.wide.ItemTier1WideMiningLaser;
import com.gekox.gekosLasers.items.lasers.wide.ItemTier2WideMiningLaser;
import com.gekox.gekosLasers.items.lasers.wide.ItemTier3WideMiningLaser;
import com.gekox.gekosLasers.items.lasers.wide.ItemTier4WideMiningLaser;
import com.gekox.gekosLasers.items.tools.*;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;

//@GameRegistry.ObjectHolder(Reference.MODID)
public class ModItems {

	// All items
	//===================
	
	// Basic items
	public static final Item itemLaserite = new ItemLaserite();
	public static final Item itemEnergizedLaserite = new ItemEnergizedLaserite();
	public static final Item itemRefinedLaserite = new ItemRefinedLaserite();
	public static final Item itemLaserReactor = new ItemLaserReactor();
	
	// Tools
	public static final Item itemLaseriteAxe = new ItemLaseriteAxe();
	public static final Item itemLaseriteHoe = new ItemLaseriteHoe();
	public static final Item itemLaseritePickaxe = new ItemLaseritePickaxe();
	public static final Item itemLaseriteShovel = new ItemLaseriteShovel();
	public static final Item itemLaseriteSword = new ItemLaseriteSword();
	
	// Laser things
	public static final Item itemTier1LaserCasing = new ItemTier1LaserCasing();
	public static final Item itemTier2LaserCasing = new ItemTier2LaserCasing();
	public static final Item itemTier3LaserCasing = new ItemTier3LaserCasing();
	public static final Item itemTier4LaserCasing = new ItemTier4LaserCasing();
	
	public static final Item itemTier1LaserRifle = new ItemTier1LaserRifle();
	public static final Item itemTier1MiningLaser = new ItemTier1MiningLaser();
	public static final Item itemTier1WideMiningLaser = new ItemTier1WideMiningLaser();
	public static final Item itemTier1ExplosiveLaser = new ItemTier1ExplosiveLaser();
	public static final Item itemTier1EnderLaser = new ItemTier1EnderLaser();
	public static final Item itemTier1FireLaser = new ItemTier1FireLaser();
	
	public static final Item itemTier2LaserRifle = new ItemTier2LaserRifle();
	public static final Item itemTier2MiningLaser = new ItemTier2MiningLaser();
	public static final Item itemTier2WideMiningLaser = new ItemTier2WideMiningLaser();
	public static final Item itemTier2ExlposiveLaser = new ItemTier2ExplosiveLaser();
	public static final Item itemTier2EnderLaser = new ItemTier2EnderLaser();
	public static final Item itemTier2FireLaser = new ItemTier2FireLaser();
	
	public static final Item itemTier3LaserRifle = new ItemTier3LaserRifle();
	public static final Item itemTier3MiningLaser = new ItemTier3MiningLaser();
	public static final Item itemTier3WideMiningLaser = new ItemTier3WideMiningLaser();
	public static final Item itemTier3ExlposiveLaser = new ItemTier3ExplosiveLaser();
	public static final Item itemTier3EnderLaser = new ItemTier3EnderLaser();
	public static final Item itemTier3FireLaser = new ItemTier3FireLaser();
	
	public static final Item itemTier4LaserRifle = new ItemTier4LaserRifle();
	public static final Item itemTier4MiningLaser = new ItemTier4MiningLaser();
	public static final Item itemTier4WideMiningLaser = new ItemTier4WideMiningLaser();
	public static final Item itemTier4ExlposiveLaser = new ItemTier4ExplosiveLaser();
	public static final Item itemTier4EnderLaser = new ItemTier4EnderLaser();
	public static final Item itemTier4FireLaser = new ItemTier4FireLaser();
	
	// Swords
	public static final Item itemSwordHilt = new ItemSwordHilt();
	public static final Item itemTier1LaserSword = new ItemTier1LaserSword(new ItemStack(itemSwordHilt, 1, 0));
	public static final Item itemTier2LaserSword = new ItemTier2LaserSword(new ItemStack(itemSwordHilt, 1, 1));
	public static final Item itemTier3LaserSword = new ItemTier3LaserSword(new ItemStack(itemSwordHilt, 1, 2));
	public static final Item itemTier4LaserSword = new ItemTier4LaserSword(new ItemStack(itemSwordHilt, 1, 3));
	
	public static final void init() {
		
		// Basic items
		GameRegistry.registerItem(itemLaserite, Names.Items.LASERITE);
		GameRegistry.registerItem(itemEnergizedLaserite, Names.Items.ENERGIZED_LASERITE);
		GameRegistry.registerItem(itemRefinedLaserite, Names.Items.REFINED_LASERITE);
		GameRegistry.registerItem(itemLaserReactor, Names.Items.LASER_REACTOR);
		
		// Tools
		GameRegistry.registerItem(itemLaseriteSword, Names.Items.LASERITE_SWORD);
		GameRegistry.registerItem(itemLaseriteAxe, Names.Items.LASERITE_AXE);
		GameRegistry.registerItem(itemLaseriteHoe, Names.Items.LASERITE_HOE);
		GameRegistry.registerItem(itemLaseritePickaxe, Names.Items.LASERITE_PICKAXE);
		GameRegistry.registerItem(itemLaseriteShovel, Names.Items.LASERITE_SHOVEL);
		
		// Laser things
		GameRegistry.registerItem(itemTier1LaserRifle, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[0]);
		GameRegistry.registerItem(itemTier1MiningLaser, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[1]);
		GameRegistry.registerItem(itemTier1WideMiningLaser, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[2]);
		GameRegistry.registerItem(itemTier1ExplosiveLaser, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[3]);
		GameRegistry.registerItem(itemTier1EnderLaser, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[4]);
		GameRegistry.registerItem(itemTier1FireLaser, Names.Items.LASER_UPGRADE_NAMES[0] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[5]);
		
		GameRegistry.registerItem(itemTier2LaserRifle, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[0]);
		GameRegistry.registerItem(itemTier2MiningLaser, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[1]);
		GameRegistry.registerItem(itemTier2WideMiningLaser, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[2]);
		GameRegistry.registerItem(itemTier2ExlposiveLaser, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[3]);
		GameRegistry.registerItem(itemTier2EnderLaser, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[4]);
		GameRegistry.registerItem(itemTier2FireLaser, Names.Items.LASER_UPGRADE_NAMES[1] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[5]);
		
		GameRegistry.registerItem(itemTier3LaserRifle, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[0]);
		GameRegistry.registerItem(itemTier3MiningLaser, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[1]);
		GameRegistry.registerItem(itemTier3WideMiningLaser, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[2]);
		GameRegistry.registerItem(itemTier3ExlposiveLaser, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[3]);
		GameRegistry.registerItem(itemTier3EnderLaser, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[4]);
		GameRegistry.registerItem(itemTier3FireLaser, Names.Items.LASER_UPGRADE_NAMES[2] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[5]);
		
		GameRegistry.registerItem(itemTier4LaserRifle, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[0]);
		GameRegistry.registerItem(itemTier4MiningLaser, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[1]);
		GameRegistry.registerItem(itemTier4WideMiningLaser, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[2]);
		GameRegistry.registerItem(itemTier4ExlposiveLaser, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[3]);
		GameRegistry.registerItem(itemTier4EnderLaser, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[4]);
		GameRegistry.registerItem(itemTier4FireLaser, Names.Items.LASER_UPGRADE_NAMES[3] + "_" +  Names.Items.LASER_BASENAME + "_" + Names.Items.LASER_TYPES[5]);
		
		// Cases
		GameRegistry.registerItem(itemTier1LaserCasing, Names.Items.LASER_UPGRADE_NAMES[0] + "_" + Names.Items.CASE_BASENAME);
		GameRegistry.registerItem(itemTier2LaserCasing, Names.Items.LASER_UPGRADE_NAMES[1] + "_" + Names.Items.CASE_BASENAME);
		GameRegistry.registerItem(itemTier3LaserCasing, Names.Items.LASER_UPGRADE_NAMES[2] + "_" + Names.Items.CASE_BASENAME);
		GameRegistry.registerItem(itemTier4LaserCasing, Names.Items.LASER_UPGRADE_NAMES[3] + "_" + Names.Items.CASE_BASENAME);
		
		// Swords
		GameRegistry.registerItem(itemSwordHilt, Names.Items.HILT_BASENAME);
		GameRegistry.registerItem(itemTier1LaserSword, Names.Items.LASER_UPGRADE_NAMES[0] + "_" + Names.Items.LASER_SWORD_BASENAME);
		GameRegistry.registerItem(itemTier2LaserSword, Names.Items.LASER_UPGRADE_NAMES[1] + "_" + Names.Items.LASER_SWORD_BASENAME);
		GameRegistry.registerItem(itemTier3LaserSword, Names.Items.LASER_UPGRADE_NAMES[2] + "_" + Names.Items.LASER_SWORD_BASENAME);
		GameRegistry.registerItem(itemTier4LaserSword, Names.Items.LASER_UPGRADE_NAMES[3] + "_" + Names.Items.LASER_SWORD_BASENAME);
		
		
	}
}
