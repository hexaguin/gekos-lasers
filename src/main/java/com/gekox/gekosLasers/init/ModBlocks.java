package com.gekox.gekosLasers.init;

import com.gekox.gekosLasers.blocks.*;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityRelay;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurret;
import com.gekox.gekosLasers.blocks.tileEntity.TileEntityTurretSphere;
import com.gekox.gekosLasers.items.ItemMetaBlockGL;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraftforge.oredict.OreDictionary;

//@GameRegistry.ObjectHolder(Reference.MODID)
public class ModBlocks {

	// All blocks
	public static Block laserOreBlock = new BlockLaseriteOre();
	public static Block laserStorageBlock = new LaserStorageBlock();
	public static Block crystalBlockUnlit = new BlockLaseriteCrystal(false);
	public static Block crystalBlockLit = new BlockLaseriteCrystal(true);
	
	public static Block redstoneCrystalBlockUnlit = new BlockLaseriteRedstoneCrystal(false);
	public static Block redstoneCrystalBlockLit = new BlockLaseriteRedstoneCrystal(true);
	
	public static Block turretBlock = new BlockTurret();
	public static Block turretSphereBlock = new BlockTurretSphere();
	
	public static Block relayBlock = new BlockRelay();
		
	public static final void init() {

		// Register all blocks
		
		GameRegistry.registerBlock(laserOreBlock, Names.Blocks.LASERITE_ORE);
		GameRegistry.registerBlock(laserStorageBlock, Names.Blocks.LASERITE_BLOCK);
		
		GameRegistry.registerBlock(crystalBlockUnlit, Names.Blocks.LASERITE_CRYSTAL + "Unlit");
		GameRegistry.registerBlock(crystalBlockLit, Names.Blocks.LASERITE_CRYSTAL + "Lit");
		
		GameRegistry.registerBlock(redstoneCrystalBlockUnlit, Names.Blocks.LASERITE_REDSTONE_CRYSTAL + "Unlit");
		GameRegistry.registerBlock(redstoneCrystalBlockLit, Names.Blocks.LASERITE_REDSTONE_CRYSTAL + "Lit");
		
		GameRegistry.registerBlock(turretBlock, Names.Blocks.TURRET_BLOCK);
		GameRegistry.registerTileEntity(TileEntityTurret.class, Names.Blocks.TURRET_BLOCK + "TileEntity");
		
		GameRegistry.registerBlock(turretSphereBlock, Names.Blocks.TURRET_BLOCK_SPHERE);
		GameRegistry.registerTileEntity(TileEntityTurretSphere.class, Names.Blocks.TURRET_BLOCK_SPHERE + "TileEntity");
		
		GameRegistry.registerBlock(relayBlock, Names.Blocks.LASER_RELAY);
		GameRegistry.registerTileEntity(TileEntityRelay.class, Names.Blocks.LASER_RELAY + "TileEntity");
		
	}

}
