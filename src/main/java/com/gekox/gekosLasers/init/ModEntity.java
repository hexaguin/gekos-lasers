package com.gekox.gekosLasers.init;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.Entity;

import com.gekox.gekosLasers.entity.projectile.laser.*;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;
import com.gekox.gekosLasers.utility.LogHelper;

import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

//@GameRegistry.ObjectHolder(Reference.MODID)
public class ModEntity {

	static List<Class<? extends Entity>> classes = new ArrayList<Class<? extends Entity>>();
	static List<String> names = new ArrayList<String>();
	
	// All entities
		
	public static final void init() {

		LogHelper.info("Registering entities");
		
//		classes.add(ProjectileLaser.class);
//		names.add(Names.Entities.PROJECTILE_LASER);
		
		classes.add(ProjectileLaserBlack.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Black");
		classes.add(ProjectileLaserBlue.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Blue");
		classes.add(ProjectileLaserBrown.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Brown");
		classes.add(ProjectileLaserCyan.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Cyan");
		classes.add(ProjectileLaserGray.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Gray");
		classes.add(ProjectileLaserGreen.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Green");
		classes.add(ProjectileLaserLightBlue.class);
		names.add(Names.Entities.PROJECTILE_LASER + "LightBlue");
		classes.add(ProjectileLaserLightGray.class);
		names.add(Names.Entities.PROJECTILE_LASER + "LightGray");
		classes.add(ProjectileLaserLime.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Lime");
		classes.add(ProjectileLaserMagenta.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Magenta");
		classes.add(ProjectileLaserOrange.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Orange");
		classes.add(ProjectileLaserPink.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Pink");
		classes.add(ProjectileLaserPurple.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Purple");
		classes.add(ProjectileLaserRed.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Red");
		classes.add(ProjectileLaserWhite.class);
		names.add(Names.Entities.PROJECTILE_LASER + "White");
		classes.add(ProjectileLaserYellow.class);
		names.add(Names.Entities.PROJECTILE_LASER + "Yellow");
		
		// Register all entities
		
		for(int i = 0; i < classes.size(); i++) {
			registerEntity(classes.get(i), names.get(i), i);
		}
		
	}
	
	private static void registerEntity(Class c, String s, int i) {
		//LogHelper.info(String.format("Registering entity (%s) with name %s and id %d", c.toString(), s, i));
		EntityRegistry.registerModEntity(c, s, i, Reference.MODID, Names.Entities.LASER_TRACKING_DISTANCE, Names.Entities.LASER_UPDATE_FREQUENCY, true);
	}

}
