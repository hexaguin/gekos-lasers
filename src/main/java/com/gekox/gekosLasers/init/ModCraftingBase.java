package com.gekox.gekosLasers.init;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.ShapedOreRecipe;

import com.gekox.gekosLasers.crafting.NBTShapelessCrafting;
import com.gekox.gekosLasers.reference.ConfigSettings;
import com.gekox.gekosLasers.reference.LaserReference;
import com.gekox.gekosLasers.reference.ModReference;
import com.gekox.gekosLasers.utility.LogHelper;
import com.gekox.gekosLasers.utility.NBTHelper;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModCraftingBase {

	public static void init() {
		
		LogHelper.info("Registering recipes");
		RecipeSorter.register("gekoslasers:nbtshapeless", NBTShapelessCrafting.class, RecipeSorter.Category.SHAPELESS, "after:minecraft:shapeless");
		
		// Furnace recipes
		GameRegistry.addSmelting(new ItemStack(ModItems.itemLaserite), new ItemStack(ModItems.itemRefinedLaserite), 1.0f);
		
		// Recipes for base items that use TE recipes
		if(ModReference.MOD_THERMAL_EXPANSION && ConfigSettings.USE_TE_RECIPES) {
			ModCraftingTE.init();
		}
		else {
			ModCraftingNonTE.init();
		}
		
		// Register all crafting recipes
		
		// Storage block
		GameRegistry.addRecipe(new ItemStack(ModBlocks.laserStorageBlock), new Object[] {"OOO", "OOO", "OOO", 'O', ModItems.itemLaserite});
		GameRegistry.addShapelessRecipe(new ItemStack(ModItems.itemLaserite, 9), new Object[] {ModBlocks.laserStorageBlock});
		
		// Misc blocks
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.crystalBlockUnlit), new Object[] {"GGG", "GLG", "GGG", 'G', "blockGlass", 'L', ModItems.itemLaserite}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.redstoneCrystalBlockUnlit), new Object[] {"GGG", "GLG", "GGG", 'G', "blockGlass", 'L', ModItems.itemEnergizedLaserite}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.relayBlock), new Object[] {"SSS", "SL ", "SSS", 'S', "stone", 'L', ModItems.itemEnergizedLaserite}));
					
		// Tools
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteAxe), new Object[] {"LL ", "LS ", " S ", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		//GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteAxe), new Object[] {" LL", " SL", " S ", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteHoe), new Object[] {"LL ", " S ", " S ", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		//GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteHoe), new Object[] {" LL", " S ", " S ", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseritePickaxe), new Object[] {"LLL", " S ", " S ", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteShovel), new Object[] {"L", "S", "S", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemLaseriteSword), new Object[] {"L", "L", "S", 'L', ModItems.itemLaserite, 'S', "stickWood"}));
		
		// Refined laserite colors
		for(int i = 0; i < 16; i++) {
			for(int j = 0; j < 16; j++) {
				GameRegistry.addShapelessRecipe(new ItemStack(ModItems.itemRefinedLaserite, 1, i), new Object[] {new ItemStack(Items.dye, 1, 15-i), new ItemStack(ModItems.itemRefinedLaserite, 1, j)});
			}
		}
		
		// References to things
		ItemStack laserReactorLeadstone = new ItemStack(ModItems.itemLaserReactor, 1, 0);
		ItemStack laserReactorHardened = new ItemStack(ModItems.itemLaserReactor, 1, 1);
		ItemStack laserReactorRedstone = new ItemStack(ModItems.itemLaserReactor, 1, 2);
		ItemStack laserReactorResonant = new ItemStack(ModItems.itemLaserReactor, 1, 3);
		
		// Case crafting
		
		// Rifle
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 0), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Items.bow}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 0), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 0)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 0), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 0)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 0), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 0)}));
		
		// Precision Mining laser
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 1), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Items.iron_pickaxe}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 1), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 1)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 1), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 1)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 1), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 1)}));
		
		// Wide Mining Laser
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 2), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Items.diamond_pickaxe}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 2), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 2)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 2), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 2)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 2), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 2)}));
		
		// Explosive Laser
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 3), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Blocks.tnt}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 3), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 3)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 3), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 3)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 3), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 3)}));
		
		// Ender Laser
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 4), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Items.ender_pearl}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 4), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 4)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 4), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 4)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 4), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 4)}));
		
		// Fire Laser
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier1LaserCasing, 1, 5), new Object[] {"#I ", "IRI", " II", 'I', "ingotLead", 'R', laserReactorLeadstone, '#', Items.flint_and_steel}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier2LaserCasing, 1, 5), new Object[] {"#I ", "IRI", " II", 'I', "ingotInvar", 'R', laserReactorHardened, '#', new ItemStack(ModItems.itemTier1LaserCasing, 1, 5)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier3LaserCasing, 1, 5), new Object[] {"#I ", "IRI", " II", 'I', "ingotElectrum", 'R', laserReactorRedstone, '#', new ItemStack(ModItems.itemTier2LaserCasing, 1, 5)}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemTier4LaserCasing, 1, 5), new Object[] {"#I ", "IRI", " II", 'I', "ingotEnderium", 'R', laserReactorResonant, '#', new ItemStack(ModItems.itemTier3LaserCasing, 1, 5)}));
		
		// Sword hilts
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemSwordHilt, 1, 0), new Object[] {"IRI", " I ", 'I', "ingotLead", 'R', laserReactorLeadstone}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemSwordHilt, 1, 1), new Object[] {"IRI", " I ", 'I', "ingotInvar", 'R', laserReactorHardened}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemSwordHilt, 1, 2), new Object[] {"IRI", " I ", 'I', "ingotElectrum", 'R', laserReactorRedstone}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.itemSwordHilt, 1, 3), new Object[] {"IRI", " I ", 'I', "ingotEnderium", 'R', laserReactorResonant}));
		
		// LaserRifle crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1LaserRifle, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 0);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2LaserRifle, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 0);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3LaserRifle, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 0);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4LaserRifle, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 0);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
		}
		
		// Mining Laser crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1MiningLaser, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 1);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2MiningLaser, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 1);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3MiningLaser, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 1);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4MiningLaser, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 1);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);

		}
		
		// Wide Mining Laser crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1WideMiningLaser, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 2);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2WideMiningLaser, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 2);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3WideMiningLaser, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 2);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4WideMiningLaser, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 2);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
		}
		
		// Explosive Laser crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1ExplosiveLaser, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 3);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2ExlposiveLaser, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 3);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3ExlposiveLaser, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 3);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4ExlposiveLaser, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 3);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
			
		}
		
		// Ender Laser crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1EnderLaser, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 4);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2EnderLaser, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 4);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3EnderLaser, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 4);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4EnderLaser, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 4);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
		}
		
		// Fire Laser crafting
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1FireLaser, 1);
			ItemStack casing1 = new ItemStack(ModItems.itemTier1LaserCasing, 1, 5);
			
			ItemStack result2 = new ItemStack(ModItems.itemTier2FireLaser, 1);
			ItemStack casing2 = new ItemStack(ModItems.itemTier2LaserCasing, 1, 5);
			
			ItemStack result3 = new ItemStack(ModItems.itemTier3FireLaser, 1);
			ItemStack casing3 = new ItemStack(ModItems.itemTier3LaserCasing, 1, 5);
			
			ItemStack result4 = new ItemStack(ModItems.itemTier4FireLaser, 1);
			ItemStack casing4 = new ItemStack(ModItems.itemTier4LaserCasing, 1, 5);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, casing1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, casing2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, casing3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, casing4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
		}
		
		// Laser swords
		for(int i = 0; i < 16; i++) {
			
			ItemStack refinedLaserite = new ItemStack(ModItems.itemRefinedLaserite, 1, i);
			
			ItemStack result1 = new ItemStack(ModItems.itemTier1LaserSword, 1, i);
			ItemStack result2 = new ItemStack(ModItems.itemTier2LaserSword, 1, i);
			ItemStack result3 = new ItemStack(ModItems.itemTier3LaserSword, 1, i);
			ItemStack result4 = new ItemStack(ModItems.itemTier4LaserSword, 1, i);
			
			ItemStack hilt1 = new ItemStack(ModItems.itemSwordHilt, 1, 0);
			ItemStack hilt2 = new ItemStack(ModItems.itemSwordHilt, 1, 1);
			ItemStack hilt3 = new ItemStack(ModItems.itemSwordHilt, 1, 2);
			ItemStack hilt4 = new ItemStack(ModItems.itemSwordHilt, 1, 3);
			
			NBTHelper.setInteger(result1, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result2, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result3, LaserReference.LASER_COLOR_TAG, i);
			NBTHelper.setInteger(result4, LaserReference.LASER_COLOR_TAG, i);
			
			GameRegistry.addShapelessRecipe(result1, new Object[] {refinedLaserite, hilt1});
			NBTShapelessCrafting recipe1 = new NBTShapelessCrafting(refinedLaserite, result1, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe1);
			
			GameRegistry.addShapelessRecipe(result2, new Object[] {refinedLaserite, hilt2});
			NBTShapelessCrafting recipe2 = new NBTShapelessCrafting(refinedLaserite, result2, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe2);
			
			GameRegistry.addShapelessRecipe(result3, new Object[] {refinedLaserite, hilt3});
			NBTShapelessCrafting recipe3 = new NBTShapelessCrafting(refinedLaserite, result3, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe3);
			
			GameRegistry.addShapelessRecipe(result4, new Object[] {refinedLaserite, hilt4});
			NBTShapelessCrafting recipe4 = new NBTShapelessCrafting(refinedLaserite, result4, LaserReference.LASER_COLOR_TAG, i);
			GameRegistry.addRecipe(recipe4);
			
		}
	}
}
