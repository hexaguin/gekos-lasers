package com.gekox.gekosLasers.crafting;

import java.util.*;

import com.gekox.gekosLasers.utility.NBTHelper;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class NBTShapelessCrafting implements IRecipe {

	private final ItemStack output;
	private final ItemStack input;
	private final String NBTKey;
	private final int NBTValue;
	private boolean hasValidNBT = false;

	public NBTShapelessCrafting(ItemStack output, ItemStack input, String key, int value) {
		this.output = output;
		this.input = input;
		this.NBTKey = key;
		this.NBTValue = value;
	}
	
	@Override
	public boolean matches(InventoryCrafting invCrafting, World world) {
		
		ArrayList<ItemStack> itemList = new ArrayList<ItemStack>();
		itemList.add(input);
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				
				ItemStack itemStack = invCrafting.getStackInRowAndColumn(j, i);

				if(itemStack != null) {

					boolean flag = false;
					Iterator iterator = itemList.iterator();
					
					while(iterator.hasNext()) {
						
						ItemStack inputStack = (ItemStack)iterator.next();
						this.hasValidNBT = NBTHelper.hasTag(itemStack, NBTKey);
						
						if(this.hasValidNBT && (itemStack.getItem() == inputStack.getItem())) {
							
							int value = NBTHelper.getInt(itemStack, NBTKey);
							if(value == this.NBTValue) {
								flag = true;
								itemList.remove(inputStack);
								break;
							}
						}
					}
					
					if (!flag) {
						return false;
					}
				}
			}
		}
		
		//System.out.println("Crafting: " + didSucceed);
		return itemList.isEmpty();
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting p_77572_1_) {
		return this.output.copy();
	}

	@Override
	public int getRecipeSize() {
		//return this.recipe.size();
		return 1;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return this.output;
	}

}
