package com.gekox.gekosLasers.proxy;

import net.minecraftforge.common.MinecraftForge;

import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.init.ModCraftingBase;
import com.gekox.gekosLasers.init.ModEntity;
import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.init.ModOreDict;
import com.gekox.gekosLasers.init.ModWorldGen;
import com.gekox.gekosLasers.items.ItemLaserSword;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

// Common proxy. Anything that is shared between client and server should go here

public abstract class CommonProxy implements IProxy {

	public void preInit(FMLPreInitializationEvent e) {
		
	}
	
	public void init(FMLInitializationEvent e) {
		ModItems.init();
		ModBlocks.init();
		ModOreDict.init();
		ModEntity.init();
		ModCraftingBase.init();
		ModWorldGen.init();
		//MinecraftForge.EVENT_BUS.register(new ItemLaserSword());
	}
	
	public void postInit(FMLPostInitializationEvent e) {
		
	}
	
}
