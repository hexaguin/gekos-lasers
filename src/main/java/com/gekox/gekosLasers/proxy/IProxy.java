package com.gekox.gekosLasers.proxy;

import com.gekox.gekosLasers.init.ModBlocks;
import com.gekox.gekosLasers.init.ModCraftingBase;
import com.gekox.gekosLasers.init.ModItems;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

// Interface to enforce the proxy methods

public interface IProxy {

	// Pre init
	public abstract void preInit(FMLPreInitializationEvent e);
	
	// Init
	public abstract void init(FMLInitializationEvent e);
	
	// Post init
	public abstract void postInit(FMLPostInitializationEvent e);
	
	// Regsiters key bindings
	public abstract void registerKeyBindings();
	
	public abstract void initRenderingAndTextures();
}
