package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Gray laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserGray extends ProjectileLaser {
	
	public ProjectileLaserGray(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.GRAY);
	}
	
	public ProjectileLaserGray(World world) {
		super(world, Color.GRAY);
	}
}
