package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Cyan laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserCyan extends ProjectileLaser {
	
	public ProjectileLaserCyan(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.CYAN);
	}
	
	public ProjectileLaserCyan(World world) {
		super(world, Color.CYAN);
	}
}
