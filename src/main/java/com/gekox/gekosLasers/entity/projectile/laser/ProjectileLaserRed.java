package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Red laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserRed extends ProjectileLaser {
	
	public ProjectileLaserRed(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.RED);
	}
	
	public ProjectileLaserRed(World world) {
		super(world, Color.RED);
	}
}
