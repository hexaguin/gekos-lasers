package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Green laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserGreen extends ProjectileLaser {
	
	public ProjectileLaserGreen(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.GREEN);
	}
	
	public ProjectileLaserGreen(World world) {
		super(world, Color.GREEN);
	}
}
