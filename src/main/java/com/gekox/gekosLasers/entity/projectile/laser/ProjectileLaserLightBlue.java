package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Light blue laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserLightBlue extends ProjectileLaser {
	
	public ProjectileLaserLightBlue(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.LIGHT_BLUE);
	}
	
	public ProjectileLaserLightBlue(World world) {
		super(world, Color.LIGHT_BLUE);
	}
}
