package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Lime laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserLime extends ProjectileLaser {
	
	public ProjectileLaserLime(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.LIME);
	}
	
	public ProjectileLaserLime(World world) {
		super(world, Color.LIME);
	}
}
