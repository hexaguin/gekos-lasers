package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Black laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserBlack extends ProjectileLaser {
	
	public ProjectileLaserBlack(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.BLACK);
	}
	
	public ProjectileLaserBlack(World world) {
		super(world, Color.BLACK);
	}
}
