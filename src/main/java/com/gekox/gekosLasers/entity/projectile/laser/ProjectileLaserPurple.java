package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Purple laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserPurple extends ProjectileLaser {
	
	public ProjectileLaserPurple(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.PURPLE);
	}
	
	public ProjectileLaserPurple(World world) {
		super(world, Color.PURPLE);
	}
}
