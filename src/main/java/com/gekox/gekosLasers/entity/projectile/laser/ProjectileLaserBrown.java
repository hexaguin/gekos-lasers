package com.gekox.gekosLasers.entity.projectile.laser;

import com.gekox.gekosLasers.utility.Color;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Brown laser
 * 
 * @author Xavier Hunt
 *
 */

public class ProjectileLaserBrown extends ProjectileLaser {
	
	public ProjectileLaserBrown(World world, EntityLivingBase entityBase) {
		super(world, entityBase, Color.BROWN);
	}
	
	public ProjectileLaserBrown(World world) {
		super(world, Color.BROWN);
	}
}
