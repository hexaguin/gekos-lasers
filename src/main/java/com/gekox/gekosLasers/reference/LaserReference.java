package com.gekox.gekosLasers.reference;

import com.gekox.gekosLasers.utility.Color;

public class LaserReference {

	public static final String LASER_TEXTURE = "textures/entities/laser.png";
	
	public static final String PARTICLE_TEXTURE = "textures/entities/laserParticle.png";
	public static final String PARTICLE_TEXTURE_SQUARE = "textures/entities/squareParticle.png";
	public static final String SWORD_BEAM_TEXTURE = "textures/misc/swordBeam.png";
	public static final String TIER_1_SWORD_HILT_TEXTURE = "textures/misc/tier1_laserSwordHilt.png";
	public static final String TIER_2_SWORD_HILT_TEXTURE = "textures/misc/tier2_laserSwordHilt.png";
	public static final String TIER_3_SWORD_HILT_TEXTURE = "textures/misc/tier3_laserSwordHilt.png";
	public static final String TIER_4_SWORD_HILT_TEXTURE = "textures/misc/tier4_laserSwordHilt.png";
	
	public static final String LASER_COLOR_TAG = "LaserColor";
	public static final String SWORD_ACTIVE_TAG = "SwordActive";
	
	// Default values
	public static int DEFAULT_LASER_HARDNESS = -1;
	
	public static float DEFAULT_RIFLE_COOLDOWN_MODIFER = 0.5f;
	public static float DEFAULT_RIFLE_DAMAGE_MODIFER = 6.0f;
	public static float DEFAULT_RIFLE_ENERGY_MODIFER = 1.0f;
	
	public static float DEFAULT_MINING_COOLDOWN_MODIFER = 1.0f;
	public static float DEFAULT_MINING_DAMAGE_MODIFER = 0.5f;
	public static float DEFAULT_MINING_ENERGY_MODIFER = 1.0f;
	
	public static float DEFAULT_WIDE_COOLDOWN_MODIFER = 2.5f;
	public static float DEFAULT_WIDE_DAMAGE_MODIFER = 3.0f;
	public static float DEFAULT_WIDE_ENERGY_MODIFER = 4.0f;
	
	public static float DEFAULT_EXPLOSIVE_COOLDOWN_MODIFER = 2.5f;
	public static float DEFAULT_EXPLOSIVE_DAMAGE_MODIFER = 4.0f;
	public static float DEFAULT_EXPLOSIVE_ENERGY_MODIFER = 5.0f;
	
	public static float DEFAULT_ENDER_COOLDOWN_MODIFER = 2.5f;
	public static float DEFAULT_ENDER_DAMAGE_MODIFER = 0.5f;
	public static float DEFAULT_ENDER_ENERGY_MODIFER = 10.0f;
	
	public static float DEFAULT_FIRE_COOLDOWN_MODIFER = 2.5f;
	public static float DEFAULT_FIRE_DAMAGE_MODIFER = 4.0f;
	public static float DEFAULT_FIRE_ENERGY_MODIFER = 5.0f;
	
	public static float DEFAULT_SWORD_DAMAGE_MODIFER = 6.0f;
	public static float DEFAULT_SWORD_REFLECT_DAMAGE_MODIFER = 2.0f;
	
	public static int DEFAULT_TIER_1_MAX_ENERGY = 100000;
	public static int DEFAULT_TIER_1_BASE_ENERGY_USE = 200;
	public static int DEFAULT_TIER_1_BASE_COOLDOWN = 20;
	public static int DEFAULT_TIER_1_BASE_DAMAGE = 1;
	
	public static int DEFAULT_TIER_2_MAX_ENERGY = 400000;
	public static int DEFAULT_TIER_2_BASE_ENERGY_USE = 200;
	public static int DEFAULT_TIER_2_BASE_COOLDOWN = 20;
	public static int DEFAULT_TIER_2_BASE_DAMAGE = 2;
	
	public static int DEFAULT_TIER_3_MAX_ENERGY = 400000;
	public static int DEFAULT_TIER_3_BASE_ENERGY_USE = 200;
	public static int DEFAULT_TIER_3_BASE_COOLDOWN = 10;
	public static int DEFAULT_TIER_3_BASE_DAMAGE = 3;
	
	public static int DEFAULT_TIER_4_MAX_ENERGY = 500000;
	public static int DEFAULT_TIER_4_BASE_ENERGY_USE = 100;
	public static int DEFAULT_TIER_4_BASE_COOLDOWN = 10;
	public static int DEFAULT_TIER_4_BASE_DAMAGE = 4;
	
	// Flavor text
	public static String FLAVOR_ENDER = "High tech enderman";
	public static String FLAVOR_EXPLOSIVE = "BOOM!";
	public static String FLAVOR_FIRE = "The Deforester";
	public static String FLAVOR_MINING = "Anti-block beam";
	public static String FLAVOR_RIFLE = "XVL1456";
	public static String FLAVOR_WIDE = "Portable hole generator";
	public static String FLAVOR_SWORD = "Totally not from a far away galaxy";
	
	
}
