package com.gekox.gekosLasers.reference.tools;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

import com.gekox.gekosLasers.reference.Names;

// Reference class for tools made from laserite

public class LaseriteTools {

	// Tool material values
	public static final int HARVESTLEVEL = 4;
	public static final int DURABILITY = 63;
	public static final float MININGSPEED = 12.0f;
	public static final float DAMAGE = 4.0f;
	public static final int ENCHANTABILITY = 12;
	
	public static final ToolMaterial LASERITE_TOOL_MATERIAL = EnumHelper.addToolMaterial(Names.Items.LASERITE_TOOL_MATERIAL, 
			LaseriteTools.HARVESTLEVEL, 
			LaseriteTools.DURABILITY, 
			LaseriteTools.MININGSPEED, 
			LaseriteTools.DAMAGE, 
			LaseriteTools.ENCHANTABILITY);
	
}
