package com.gekox.gekosLasers.reference;

// Reference class

public class Reference {
	
	// Reference strings
	public static final String MODID = "GekosLasers";
	public static final String RESOURCE_PREFIX = MODID.toLowerCase() + ":";
	public static final String RESOURCE_PREFIX_NO_COLON = MODID.toLowerCase();
	public static final String NAME = "Geko's Lasers";
	public static final String VERSION = "1.7.10R0.8";
	public static final String SERVER_PROXY_CLASS = "com.gekox.gekosLasers.proxy.ServerProxy";
	public static final String CLIENT_PROXY_CLASS = "com.gekox.gekosLasers.proxy.ClientProxy";
	public static final String GUI_FACTORY_CLASS = "com.gekox.gekosLasers.client.gui.GuiFactory";
	public static final String DEPENDENCIES = "after:ThermalFoundation;after:ThermalExpansion";
	
	public static final String DOWNLOAD_URL = "http://minecraft.curseforge.com/mc-mods/228888-gekos-lasers/files";
	public static final String RELEASE_URL = "https://bitbucket.org/Geko_X/gekos-lasers/raw/master/VERSION";
	
	public static final String LASER_CONFIG_GENERAL = "General Laser Settings";
	public static final String LASER_CONFIG_TIER_1 = "Tier 1 Laser Settings";
	public static final String LASER_CONFIG_TIER_2 = "Tier 2 Laser Settings";
	public static final String LASER_CONFIG_TIER_3 = "Tier 3 Laser Settings";
	public static final String LASER_CONFIG_TIER_4 = "Tier 4 Laser Settings";
	
	public static final String RELAY_CONFIG = "Laser Relay Settings";
	
	public static final String LASER_DAMAGE_SOURCE_NAME = "gekoslasers.laser";
	public static final String SWORD_DAMAGE_SOURCE_NAME = "gekoslasers.laserSword";
	
}
