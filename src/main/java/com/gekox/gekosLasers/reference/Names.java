package com.gekox.gekosLasers.reference;

public final class Names {

	// Blocks
	public static final class Blocks {
		public static final String LASERITE_ORE = "laseriteOre";
		public static final String LASERITE_BLOCK = "laseriteBlock";
		public static final String LASERITE_CRYSTAL = "laseriteCrystal";
		public static final String LASERITE_REDSTONE_CRYSTAL = "laseriteRedstoneCrystal";
		public static final String LASER_RELAY = "laserRelay";
		
		public static final String TURRET_BLOCK = "turret";
		public static final String TURRET_BLOCK_SPHERE = "turretSphere";
	}
	
	// Items
	public static final class Items {
		public static final String LASERITE_TOOL_MATERIAL = "Laserite";
		public static final String LASERITE = "laserite";
		public static final String REFINED_LASERITE = "refinedLaserite";
		public static final String ENERGIZED_LASERITE = "energizedLaserite";
		public static final String LASER_REACTOR = "laserReactor";
		public static final String[] LASER_UPGRADE_NAMES = {"tier1", "tier2", "tier3", "tier4"};
		public static final String[] LASER_TYPES = {"rifle", "mining", "wide", "explosive", "ender", "fire"};//, "silk"};
		
		public static final String LASERITE_SWORD = "laseriteSword";
		public static final String LASERITE_PICKAXE = "laseritePickaxe";
		public static final String LASERITE_AXE = "laseriteAxe";
		public static final String LASERITE_SHOVEL = "laseriteShovel";
		public static final String LASERITE_HOE = "laseriteHoe";
		
		public static final String LASER_PICKAXE = "laserPickaxe";
		
		public static final String LASER_BASENAME = "laser";
		public static final String CASE_BASENAME = "laserCasing";
				
		public static final String LASER_SWORD_BASENAME = "laserSword";
		public static final String HILT_BASENAME = "swordHilt";
		
	}
	
	// Entities
	public static final class Entities {
		public static final String PROJECTILE_LASER = "projectileLaser";
		public static final int LASER_TRACKING_DISTANCE = 100;
		public static final int LASER_UPDATE_FREQUENCY = 20;
	}
	
	// Keys
	public static final class Keys {
		
		public static final String CATEGORY = "keys.gekoslasers.category";
		public static final String LASER_SIZE = "keys.gekoslasers.laserSize";
		public static final String LASER_DEPTH = "keys.gekoslasers.laserDepth";
		
	}
	
	
}
