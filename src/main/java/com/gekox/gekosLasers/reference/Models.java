package com.gekox.gekosLasers.reference;

import com.gekox.gekosLasers.utility.ResourceLocationHelper;

import net.minecraft.util.ResourceLocation;

public class Models {

	public static final String MODEL_LOCATION = "models/";
	
	public static final ResourceLocation TURRET = ResourceLocationHelper.getResourceLocation(MODEL_LOCATION + "turret.obj");
	public static final ResourceLocation TURRET_SPHERE = ResourceLocationHelper.getResourceLocation(MODEL_LOCATION + "turretSphere.obj");
	public static final ResourceLocation RELAY = ResourceLocationHelper.getResourceLocation(MODEL_LOCATION + "relay.obj");

}
