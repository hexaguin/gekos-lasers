package com.gekox.gekosLasers.creativeTab;

import com.gekox.gekosLasers.init.ModItems;
import com.gekox.gekosLasers.reference.Names;
import com.gekox.gekosLasers.reference.Reference;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

// Creative tab for Geko's Lasers

public class CreativeTabGL {

	public static final CreativeTabs GL_TAB = new CreativeTabs(Reference.MODID.toLowerCase()) {
		
		@Override
		public Item getTabIconItem() {
			return ModItems.itemRefinedLaserite;
		}
	};
}
